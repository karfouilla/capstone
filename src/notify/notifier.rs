use std::cell::RefCell;
use crate::ecore::feature::AttributeFeature;

use crate::notify::notification::Notification;

/// Alias type for dynamic [`AdapterFn`].
pub type AdapterFnDyn = Box<dyn for<'a> Fn(
    &'a dyn ObservableDyn,
    Option<&'a AttributeFeature>,
    &'a Notification
) + 'static>;
/// Alias type for dynamic [`AdapterFnMut`].
pub type AdapterFnMutDyn = RefCell<Box<dyn for<'a> FnMut(
    &'a dyn ObservableDyn,
    Option<&'a AttributeFeature>,
    &'a Notification
) + 'static>>;
/// Registration identifiant of [`AdapterFn`] or [`AdapterFnMut`],
/// unique in the same [`Observable`] instance.
pub type AdapterId = usize;

/// An object that can be observable.
///
/// When object is observable,
/// it can registers closures ([`AdapterFn`]/[`AdapterFnMut`])
/// and send [`Notification`] to registered closures.
///
/// This trait is [object safety], therefore, [`register_mut_dyn`],
/// [`register_dyn`] and [`notify_dyn`] have dynamic parameters.
/// However there are their counterparts in [`Observable`] trait, which
/// is not [object safety] and use generic parameters instead.
///
/// # Note
/// When you implement this trait, the [`Observable`] trait is automatically
/// implemented and you cannot override its implementation.
///
/// # Derive macro
/// You can implement [`ObservableDyn`]/[`Observable`] using [`Notifier`] and
/// [derive macro `Observable`]
///
/// # Example
/// ```
/// # use capstone_macros::Observable;
/// # use capstone::ecore::data_value::Value;
/// use capstone::ecore::feature::AttributeFeature;
/// # use capstone::notify::notification::Notification;
/// # use capstone::notify::notification::NotificationEvent;
/// # use capstone::notify::notification::SetEvent;
/// # use capstone::notify::notifier::Notifier;
/// # use capstone::notify::notifier::ObservableDyn;
/// # use capstone::notify::notifier::Observable;
/// #
/// #[derive(Observable)]
/// struct Point {
///     x: i32,
///     y: i32,
///     // this is the notification manager
///     #[observable(notifier)] notifier: Notifier,
/// }
///
/// impl Point {
///     fn set_x(&mut self, x: i32) {
///         let old_x = self.x;
///         self.x = x;
///
///         let value = &Value::EInt(x);
///         let event = NotificationEvent::Set(SetEvent {
///             old_value: Some(Value::EInt(old_x)),
///             new_value: &value,
///             position: None,
///         });
///
///         self.notify(self, None, Notification::new(event));
///     }
///     fn set_y(&mut self, y: i32) {
///         let old_y = self.y;
///         self.y = y;
///
///         let value = &Value::EInt(y);
///         let event = NotificationEvent::Set(SetEvent {
///             old_value: Some(Value::EInt(old_y)),
///             new_value: &value,
///             position: None,
///         });
///
///         self.notify(self, None, Notification::new(event));
///     }
/// }
///
/// let mut point = Point {
///     x: 0,
///     y: 11,
///     notifier: Notifier::new(),
/// };
///
/// point.register(|_: &dyn ObservableDyn, feature: Option<&AttributeFeature>, notification: &Notification| {
///     println!("Point has sent an event: {:?}", notification.event())
/// });
///
/// point.set_x(42);
/// // -> send an event Set(old_value: 0, new_value: 42, position: None)
/// point.set_y(13);
/// // -> send an event Set(old_value: 11, new_value: 13, position: None)
/// ```
///
/// [object safety]: https://doc.rust-lang.org/reference/items/traits.html#object-safety
/// [`register_mut_dyn`]: Self::register_mut_dyn
/// [`register_dyn`]: Self::register_dyn
/// [`notify_dyn`]: Self::notify_dyn
/// [derive macro `Observable`]: capstone_macros::Observable
pub trait ObservableDyn {
    /// Register a new mutable closure ([`AdapterFnMutDyn`]),
    /// which will be called when a [`Notification`] is sent.
    ///
    /// This method return an identifier ([`AdapterId`])
    /// which is unique for one [`ObservableDyn`] instance.
    ///
    /// This function take dynamic closure in [`RefCell`] and [`Box`],
    /// you can use generic version [`Observable::register_mut`] that is easier
    /// if [`ObservableDyn`] object is not dynamic itself.
    fn register_mut_dyn(&mut self, adapter: AdapterFnMutDyn) -> AdapterId;
    /// Remove a mutable closure previously added with
    /// [`register_mut_dyn`] or [`register_mut`]
    ///
    /// [`register_mut_dyn`]: Self::register_mut_dyn
    /// [`register_mut`]: Observable::register_mut
    fn unregister_mut(&mut self, id: AdapterId);

    /// Register a new immutable closure ([`AdapterFnDyn`]),
    /// which will be called when a [`Notification`] is sent.
    ///
    /// This method return an identifier ([`AdapterId`])
    /// which is unique for one [`ObservableDyn`] instance.
    ///
    /// This function take dynamic closure in [`Box`],
    /// you can use generic version [`Observable::register`] that is easier
    /// if [`ObservableDyn`] object is not dynamic itself.
    fn register_dyn(&mut self, adapter: AdapterFnDyn) -> AdapterId;
    /// Remove a immutable closure previously added with
    /// [`register_dyn`] or [`register`]
    ///
    /// [`register_dyn`]: Self::register_dyn
    /// [`register`]: Observable::register
    fn unregister(&mut self, id: AdapterId);

    /// Return `true` if the notification system is enabled
    fn deliver(&self) -> bool;
    /// Enable or disable notification system
    fn set_deliver(&mut self, deliver: bool);

    /// Send [`Notification`] to all Adapter closures registered.
    ///
    /// if [`deliver`] is `false`, does nothing.
    ///
    /// `origin` parameter is the [`ObservableDyn`] from which the
    /// [`Notification`] was sent (i.e the object for which
    /// the [`Notification`] makes sense).
    ///
    /// This function take dynamic reference of `origin`,
    /// you can use generic version [`Observable::notify`] that is easier
    /// if [`ObservableDyn`] object is not dynamic itself.
    ///
    /// [`deliver`]: Self::deliver
    fn notify_dyn(
        &self,
        origin: &dyn ObservableDyn,
        feature: Option<&AttributeFeature>,
        notif: Notification
    );
}

/// Trait that extend [`ObservableDyn`] using generic parameters.
///
/// You must not and cannot implement this trait yourself :
/// this trait is automatically implemented on object
/// if [`ObservableDyn`] is implemented but it is only available
/// when object is not dynamic (see [object safety] for more information).
///
/// [object safety]: https://doc.rust-lang.org/reference/items/traits.html#object-safety
pub trait Observable: ObservableDyn {
    /// Version of [`ObservableDyn::register_mut_dyn`] using generic parameter.
    ///
    /// See [`ObservableDyn::register_mut_dyn`] for more information
    fn register_mut<T>(&mut self, adapter: T) -> AdapterId where T: for<'a>
    FnMut(&'a dyn ObservableDyn, Option<&'a AttributeFeature>, &'a Notification) + 'static;

    /// Version of [`ObservableDyn::register_dyn`] using generic parameter.
    ///
    /// See [`ObservableDyn::register_mut_dyn`] for more information
    fn register<T>(&mut self, adapter: T) -> AdapterId where T: for<'a>
    Fn(&'a dyn ObservableDyn, Option<&'a AttributeFeature>, &'a Notification) + 'static;

    /// Version of [`ObservableDyn::notify_dyn`] using generic parameter.
    ///
    /// See [`ObservableDyn::notify_dyn`] for more information
    fn notify<T: ObservableDyn>(
        &self,
        origin: &T,
        feature: Option<&AttributeFeature>,
        notif: Notification
    );
}
// auto implement for U object with ObservableDyn trait
/// Observable is automatically implemented, just implement ObservableDyn
impl<U: ObservableDyn> Observable for U {
    fn register_mut<T>(&mut self, adapter: T) -> AdapterId
    where T: for<'a> FnMut(
        &'a dyn ObservableDyn,
        Option<&'a AttributeFeature>,
        &'a Notification
    ) + 'static {
        self.register_mut_dyn(RefCell::new(Box::new(adapter)))
    }

    fn register<T>(&mut self, adapter: T) -> AdapterId
    where T: for<'a> Fn(
        &'a dyn ObservableDyn,
        Option<&'a AttributeFeature>,
        &'a Notification
    ) + 'static {
        self.register_dyn(Box::new(adapter))
    }

    fn notify<T: ObservableDyn>(
        &self,
        origin: &T,
        feature: Option<&AttributeFeature>,
        notif: Notification
    ) {
        self.notify_dyn(origin as &dyn ObservableDyn, feature, notif)
    }
}

/// Data struct that contain data for [`Observable`] objects and implement
/// itself [`Observable`] to interface with private data
pub struct Notifier {
    adapters: Vec<(usize, AdapterFnDyn)>,
    adapters_mut: Vec<(usize, AdapterFnMutDyn)>,
    deliver: bool,
    adapter_id_sequence: AdapterId,
}

impl Default for Notifier {
    fn default() -> Self {
        Self::new()
    }
}

impl Notifier {
    /// Create new empty `Notifier`, enabled but with empty adapter list.
    pub fn new() -> Self {
        Self {
            adapters: Vec::new(),
            adapters_mut: Vec::new(),
            deliver: true,
            adapter_id_sequence: 0,
        }
    }
}

impl ObservableDyn for Notifier {
    fn register_mut_dyn(&mut self, adapter: AdapterFnMutDyn) -> AdapterId {
        let id = self.adapter_id_sequence;
        self.adapter_id_sequence += 1;
        self.adapters_mut.push((id, adapter));
        id
    }
    fn unregister_mut(&mut self, id_rem: AdapterId) {
        self.adapters_mut.retain(|(id, _)| *id != id_rem)
    }

    fn register_dyn(&mut self, adapter: AdapterFnDyn) -> AdapterId {
        let id = self.adapter_id_sequence;
        self.adapter_id_sequence += 1;
        self.adapters.push((id, adapter));
        id
    }
    fn unregister(&mut self, id_rem: AdapterId) {
        self.adapters.retain(|(id, _)| *id != id_rem)
    }

    fn deliver(&self) -> bool {
        self.deliver
    }

    fn set_deliver(&mut self, deliver: bool) {
        self.deliver = deliver
    }

    fn notify_dyn(
        &self,
        origin: &dyn ObservableDyn,
        feature: Option<&AttributeFeature>,
        notification: Notification
    ) {
        if self.deliver {
            for (_, adapter_cell) in &self.adapters_mut {
                if let Ok(mut adapter) = adapter_cell.try_borrow_mut() {
                    adapter(origin, feature, &notification)
                } // else ?
            }
            for (_, adapter) in &self.adapters {
                adapter(origin, feature, &notification)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::rc::Rc;
    use super::*;

    use crate::notify::notification::{AddEvent, RemoveEvent};
    use crate::notify::notification::{NotificationEvent};
    use crate::notify::notification::{SetEvent, UnsetEvent};
    use capstone_macros::Observable;
    use crate::ecore::data_value::Value;

    #[derive(Observable)]
    #[observable(inner_crate)]
    struct ObservableStruct {
        #[observable(notifier)]
        notifier: Notifier,
        attr: i32,
        option: Option<i32>,
        list: Vec<i32>,
    }

    impl ObservableStruct {
        fn new() -> Self {
            Self {
                notifier: Notifier::new(),
                attr: 0,
                option: None,
                list: Vec::new(),
            }
        }
        fn set_attr(&mut self, attr: i32) {
            let old_attr = std::mem::replace(&mut self.attr, attr);

            let new_value = Value::EInt(attr);
            let event = NotificationEvent::Set(SetEvent {
                old_value: Some(Value::EInt(old_attr)),
                new_value: &new_value,
                position: None,
            });

            self.notify(self, None, Notification::new(event))
        }
        fn set_option(&mut self, option: i32) {
            let old_option = std::mem::replace(&mut self.option, Some(option));

            let new_value = Value::EInt(option);
            let event = NotificationEvent::Set(SetEvent {
                old_value: old_option.map(Value::EInt),
                new_value: &new_value,
                position: None,
            });

            self.notify(self, None, Notification::new(event))
        }
        fn unset_option(&mut self) {
            let old_option = std::mem::replace(&mut self.option, None);

            let event = NotificationEvent::Unset(UnsetEvent {
                old_value: old_option.map(Value::EInt),
            });

            self.notify(self, None, Notification::new(event))
        }
        fn list_insert(&mut self, position: usize, elem: i32) {
            self.list.insert(position, elem);

            let new_value = Value::EInt(elem);
            let event = NotificationEvent::Add(AddEvent {
                position,
                new_value: &new_value,
            });

            self.notify(self, None, Notification::new(event))
        }
        fn list_remove(&mut self, position: usize) {
            let old_value = self.list.remove(position);

            let event = NotificationEvent::Remove(RemoveEvent {
                position,
                old_value: Value::EInt(old_value),
            });

            self.notify(self, None, Notification::new(event))
        }
    }

    #[test]
    fn test_notifier() {
        let notif_list1: Rc<RefCell<Vec<i32>>> = Rc::new(RefCell::new(Vec::new()));
        let notif_list1_ref1 = notif_list1.clone();
        let notif_list1_ref2 = notif_list1;
        let notif_list2: Rc<RefCell<Vec<Option<i32>>>> = Rc::new(RefCell::new(Vec::new()));
        let notif_list2_ref1 = notif_list2.clone();
        let notif_list2_ref2 = notif_list2;

        let mut my_struct = ObservableStruct::new();

        let closure = move |_: &dyn ObservableDyn, _feature: Option<&AttributeFeature>, notification: &Notification| {
            if let NotificationEvent::Set(set_ev) = notification.event() {
                if let &Value::EInt(val) = set_ev.new_value {
                    notif_list1_ref1.borrow_mut().push(val);
                }
                match &set_ev.old_value {
                    None => notif_list2_ref1.borrow_mut().push(None),
                    Some(old_value) => if let &Value::EInt(val) = old_value {
                        notif_list2_ref1.borrow_mut().push(Some(val))
                    }
                }
            }
        };
        let closure_mut = move |_: &dyn ObservableDyn, _feature: Option<&AttributeFeature>, notification: &Notification| {
            if let NotificationEvent::Set(set_ev) = notification.event() {
                if let &Value::EInt(val) = set_ev.new_value {
                    notif_list1_ref2.borrow_mut().push(val);
                }
                match &set_ev.old_value {
                    None => notif_list2_ref2.borrow_mut().push(None),
                    Some(old_value) => if let &Value::EInt(val) = old_value {
                        notif_list2_ref2.borrow_mut().push(Some(val))
                    }
                }
            }
        };

        // operations (with 0 adapter)
        my_struct.set_attr(42);

        // register first Adapter
        let id1 = my_struct.register(closure);

        // operations (with 1 adapter)
        my_struct.set_attr(43);
        my_struct.set_option(49);

        // register second Adapter
        let _id2 = my_struct.register_mut(closure_mut);

        // operations (with 2 adapter)
        my_struct.list_insert(0, 12);
        my_struct.list_insert(1, 13);

        my_struct.unregister_mut(id1);

        // operations (with 1 adapter)
        my_struct.list_insert(1, 14);
        my_struct.list_remove(2);
        my_struct.unset_option();

        drop(my_struct);

        //assert_eq!(format!("{:?}", notif_list1), "RefCell { value: [Set(SetEvent { old_value: Int32(42), new_value: Int32(43), position: None }), Set(SetEvent { old_value: Unset, new_value: Int32(49), position: None }), Add(AddEvent { position: 0, new_value: Int32(12) }), Add(AddEvent { position: 1, new_value: Int32(13) }), Add(AddEvent { position: 1, new_value: Int32(14) }), Remove(RemoveEvent { position: 2, old_value: Int32(13) }), Unset(UnsetEvent { old_value: Int32(49) })] }");
        //assert_eq!(format!("{:?}", notif_list2), "RefCell { value: [Add(AddEvent { position: 0, new_value: Int32(12) }), Add(AddEvent { position: 1, new_value: Int32(13) }), Add(AddEvent { position: 1, new_value: Int32(14) }), Remove(RemoveEvent { position: 2, old_value: Int32(13) }), Unset(UnsetEvent { old_value: Int32(49) })] }");
    }
}
