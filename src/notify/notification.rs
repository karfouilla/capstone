use std::fmt::Debug;
use crate::ecore::data_value::Value;

/// Event data when value changed
#[derive(Debug)]
pub struct SetEvent<'a> {
    pub old_value: Option<Value>,
    pub new_value: &'a Value,
    pub position: Option<usize>,
}

/// Event data when value is unsetted
#[derive(Debug)]
pub struct UnsetEvent {
    pub old_value: Option<Value>,
}

/// Event data when single value added to list
#[derive(Debug)]
pub struct AddEvent<'a> {
    pub position: usize,
    pub new_value: &'a Value,
}

/// Event data when multiple values added to list
#[derive(Debug)]
pub struct AddManyEvent<'a> {
    pub position: usize,
    pub new_value: &'a [Value],
}

/// Event data when single value removed to list
#[derive(Debug)]
pub struct RemoveEvent {
    pub position: usize,
    pub old_value: Value,
}

/// Event data when multiple values removed to list
#[derive(Debug)]
pub struct RemoveManyEvent {
    pub positions: Vec<usize>,
    pub old_values: Vec<Value>,
}

/// Event data when multiple values moved in list
#[derive(Debug)]
pub struct MoveEvent<'a> {
    pub old_position: usize,
    pub new_position: usize,
    pub value: &'a Value,
}

/// The type of change that has occurred.
#[non_exhaustive]
#[derive(Debug)]
pub enum NotificationEvent<'a> {
    /// An event type indicating that the notifier has been created.
    #[deprecated]
    Create,
    /// An event type indicating that a feature of the notifier has been set.
    /// This applies for simple features.
    Set(SetEvent<'a>),
    /// An event type indicating that a feature of the notifier has been unset.
    /// This applies for unsettable features.
    Unset(UnsetEvent),
    /// An event type indicating that a value has been
    /// inserted into a list-based feature of the notifier.
    Add(AddEvent<'a>),
    /// An event type indicating that a value has been
    /// removed from a list-based feature of the notifier.
    Remove(RemoveEvent),
    /// An event type indicating that a several values
    /// have been added into a list-based feature of the notifier.
    AddMany(AddManyEvent<'a>),
    /// An event type indicating that a several values have
    /// been removed from a list-based feature of the notifier.
    RemoveMany(RemoveManyEvent),
    /// An event type indicating that a value has been
    /// moved within a list-based feature of the notifier.
    Move(MoveEvent<'a>),
    /// An event type indicating that an adapter
    /// is being removed from the notifier.
    RemovingAdapter(RemoveEvent),
    /// An event type indicating that a feature
    /// of the notifier has been resolved from a proxy.
    Resolve(SetEvent<'a>),
}

/// A description of a feature change that has occurred for some notifier.
#[derive(Debug)]
pub struct Notification<'a> {
    event: NotificationEvent<'a>,
}

impl<'a> Notification<'a> {
    /// Create new notification from [`NotificationEvent`]
    ///
    /// ```
    /// # use capstone::notify::notification::Notification;
    /// # use capstone::notify::notification::NotificationEvent;
    /// # use capstone::notify::notification::SetEvent;
    /// # use capstone::notify::notifier::{Notifier, Observable};
    /// # use capstone::ecore::data_value::Value;
    /// #
    /// # let notifier = Notifier::new();
    /// #
    /// let event = NotificationEvent::Set(SetEvent {
    ///     old_value: Some(Value::EInt(41)),
    ///     new_value: &Value::EInt(42),
    ///     position: None
    /// });
    ///
    /// let notification = Notification::new(event);
    ///
    /// notifier.notify(&notifier, None, notification)
    /// ```
    pub fn new(event: NotificationEvent<'a>) -> Self {
        Self { event }
    }
}
impl Notification<'_> {
    /// Returns the type of change that has occurred.
    ///
    /// # Deprecated
    ///
    /// use [`event`](Self::event) instead
    #[deprecated]
    pub fn event_type(&self) -> &NotificationEvent {
        &self.event
    }

    /// Returns the type and value of change that has occurred.
    pub fn event(&self) -> &NotificationEvent {
        &self.event
    }

    /// Returns the value of the notifier's feature before the change occurred.
    ///
    /// For a list-based feature, this represents a value, or a first value of
    /// list of values, removed from the list.
    ///
    /// For a move, this represents the old position of the moved value.
    ///
    /// # Deprecated
    ///
    /// use [`event`](Self::event) instead
    #[deprecated]
    pub fn old_value(&self) -> Option<&Value> {
        match &self.event {
            NotificationEvent::Set(ev) | NotificationEvent::Resolve(ev) => ev.old_value.as_ref(),
            NotificationEvent::Unset(ev) => ev.old_value.as_ref(),
            NotificationEvent::Remove(ev) | NotificationEvent::RemovingAdapter(ev) => {
                Some(&ev.old_value)
            }
            NotificationEvent::RemoveMany(ev) => ev.old_values.first(),
            //NotificationEvent::Move(ev) => Some(&Value::ELong(i64::try_from(ev.old_position).unwrap())),
            _ => None,
        }
    }

    /// Returns the value of the notifier's feature after the change occurred.
    ///
    /// For a list-based feature, this represents a value, or the first value of
    /// a list, added to the list, the first value of an array of `usize`
    /// containing the original index of each value in the list of values
    /// removed from the list (except for the case of a clear),
    /// the value moved within the list, or `None` otherwise.
    ///
    /// # Deprecated
    ///
    /// use [`event`](Self::event) instead
    #[deprecated]
    pub fn new_value(&self) -> Option<&Value> {
        match &self.event {
            NotificationEvent::Set(ev) | NotificationEvent::Resolve(ev) => Some(ev.new_value),
            NotificationEvent::Unset(_) => None,
            NotificationEvent::Add(ev) => Some(ev.new_value),
            NotificationEvent::AddMany(ev) => ev.new_value.first(),
            /*NotificationEvent::Remove(ev) | NotificationEvent::RemovingAdapter(ev) => {
                Some(NotificationValue::USize(ev.position))
            }*/
            /*NotificationEvent::RemoveMany(ev) => ev
                .positions
                .first()
                .map(|position| NotificationValue::USize(*position)),*/
            NotificationEvent::Move(ev) => Some(ev.value),
            _ => None,
        }
    }

    /// Returns whether the notifier's feature was
    /// considered set before the change occurred.
    pub fn was_set(&self) -> bool {
        match &self.event {
            NotificationEvent::Set(ev) => ev.old_value.is_some(),
            NotificationEvent::Unset(ev) => ev.old_value.is_some(),
            NotificationEvent::Add(_)
            | NotificationEvent::Remove(_)
            | NotificationEvent::AddMany(_)
            | NotificationEvent::RemoveMany(_)
            | NotificationEvent::Move(_) => true,
            _ => false,
        }
    }

    /// Returns true if this notification represents an event
    /// that did not change the state of the notifying object.
    ///
    /// For the events [`Add`], [`AddMany`],
    /// [`Remove`], [`RemoveMany`], [`Move`], it always returns false.
    ///
    /// For the events [`Resolve`] and [`RemovingAdapter`]
    /// it always returns true.
    ///
    /// For the events [`Set`] and [`Unset`] it returns
    /// true if the old and the new value are equal;
    ///
    /// In addition, for certain types of features there may be a distinction
    /// between being set to a default value and not being set at all, which
    /// implies that it has the default value.
    ///
    /// In this situation, even in the case that the old and new values are
    /// equal, isTouch may never the less return false in order to indicate
    /// that, although the value has not changed, the feature has gone from
    /// simply having a default value to being set to that same default value,
    /// or has gone from being set to the default value back to being unset.
    ///
    /// [`Add`]: NotificationEvent::Add
    /// [`AddMany`]: NotificationEvent::AddMany
    /// [`Remove`]: NotificationEvent::Remove
    /// [`RemoveMany`]: NotificationEvent::RemoveMany
    /// [`Move`]: NotificationEvent::Move
    /// [`Resolve`]: NotificationEvent::Resolve
    /// [`RemovingAdapter`]: NotificationEvent::RemovingAdapter
    /// [`Set`]: NotificationEvent::Set
    /// [`Unset`]: NotificationEvent::Unset
    pub fn is_touch(&self) -> bool {
        match &self.event {
            NotificationEvent::Add(_)
            | NotificationEvent::AddMany(_)
            | NotificationEvent::Remove(_)
            | NotificationEvent::RemoveMany(_)
            | NotificationEvent::Move(_) => false,
            NotificationEvent::Resolve(_) | NotificationEvent::RemovingAdapter(_) => true,
            NotificationEvent::Set(_ev) => /*ev.old_value.as_ref() == Some(ev.new_value)*/true,
            NotificationEvent::Unset(ev) => ev.old_value.is_none(),
            _ => false,
        }
    }

    /// Returns true if the notification's
    /// feature has been set to its default value.
    pub fn is_reset(&self) -> bool {
        match &self.event {
            NotificationEvent::Set(_) => false,
            NotificationEvent::Unset(_) => true,
            _ => false,
        }
    }

    /// Returns the position within a list-based
    /// feature at which the change occurred.
    ///
    /// It returns `None` when not applicable.
    ///
    /// # Deprecated
    ///
    /// use [`event`](Self::event) instead
    #[deprecated]
    pub fn position(&self) -> Option<usize> {
        match &self.event {
            NotificationEvent::Set(ev) | NotificationEvent::Resolve(ev) => ev.position,
            NotificationEvent::Add(ev) => Some(ev.position),
            NotificationEvent::AddMany(ev) => Some(ev.position),
            NotificationEvent::Remove(ev) | NotificationEvent::RemovingAdapter(ev) => {
                Some(ev.position)
            }
            NotificationEvent::RemoveMany(ev) => {
                // this is deprecated
                ev.positions.first().copied()
            }
            NotificationEvent::Move(ev) => Some(ev.new_position),
            _ => None,
        }
    }
/*
    /// Returns whether the notification can
    /// be and has been merged with this one.
    pub fn merge(&mut self, other: Notification) -> bool {
        let feature_eq = /*self.feature.map_or_else(|| { // self is None
            other.feature.is_none()
        }, |first| { // self is Some
            other.feature.map_or(false, // other is None
                                 |second| first.eq(second)) // other is Some
        })*/false;

        if feature_eq {
            match &mut self.event {
                NotificationEvent::Set(first) => match other.event {
                    NotificationEvent::Set(second) => {
                        first.new_value = second.new_value;
                        first.position = second.position;
                        true
                    }
                    NotificationEvent::Unset(_) => {
                        self.event = NotificationEvent::Unset(UnsetEvent {
                            old_value: first.old_value,
                        });
                        true
                    }
                    _ => false,
                },
                NotificationEvent::Unset(first) => match other.event {
                    NotificationEvent::Set(second) => {
                        self.event = NotificationEvent::Set(SetEvent {
                            old_value: first.old_value,
                            new_value: second.new_value,
                            position: second.position,
                        });
                        true
                    }
                    NotificationEvent::Unset(_) => true,
                    _ => false,
                },
                NotificationEvent::Remove(first) => match other.event {
                    NotificationEvent::Remove(second) => {
                        self.event = NotificationEvent::RemoveMany(RemoveManyEvent {
                            positions: vec![first.position, second.position],
                            old_values: vec![first.old_value, second.old_value],
                        });
                        true
                    }
                    NotificationEvent::RemoveMany(second) => {
                        self.event = NotificationEvent::RemoveMany(RemoveManyEvent {
                            positions: {
                                let mut positions = vec![first.position];
                                positions.extend(&second.positions);
                                positions
                            },
                            old_values: {
                                let mut old_values = vec![first.old_value];
                                old_values.extend(&second.old_values);
                                old_values
                            },
                        });
                        true
                    }
                    _ => false,
                },
                NotificationEvent::RemoveMany(first) => match other.event {
                    NotificationEvent::Remove(second) => {
                        first.positions.push(second.position);
                        first.old_values.push(second.old_value);
                        true
                    }
                    NotificationEvent::RemoveMany(second) => {
                        first.positions.extend(&second.positions);
                        first.old_values.extend(&second.old_values);
                        true
                    }
                    _ => false,
                },
                _ => false,
            }
        } else {
            false
        }
    }
    */
}
