use crate::ecore::data_relation::RelationReference;
use crate::ecore::data_value::{Value, ValueBounded};
use crate::ecore::debug::RefMutPrinter;
use crate::ecore::eclass::EClassRef;
use crate::ecore::error::Error;
use crate::notify::notification::{
    AddEvent, Notification, NotificationEvent, RemoveEvent, SetEvent, UnsetEvent,
};
use crate::notify::notifier::{Notifier, Observable};
use capstone_macros::Observable;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};

use super::data_relation::RelationValue;
use super::data_type::{RelationType, Type};
use super::eclass::EClass;
use super::feature::{AttributeFeature, Bound};

/// An instance of a class
#[derive(Observable)]
#[observable(inner_crate)]
pub struct EObject {
    #[observable(notifier)]
    notifier: Notifier,
    class: EClassRef,
    container: RelationReference,
    attributes: HashMap<String, ValueBounded>,
    is_valid: bool,
}

impl EObject {
    /// Create a new instance of the given class
    /// # Arguments
    /// * `class` - The class of the instance
    /// * `container` - The container of the instance (can be None through Weak)
    /// # Errors
    /// * If the class is abstract
    /// * If the class is not found
    pub fn new(class: EClassRef, container: RelationReference) -> Result<Self, Error> {
        let class_ref = class
            .upgrade()
            .ok_or_else(|| Error::new("No class or broken reference"))?;
        let class_ref = class_ref
            .try_borrow()
            .map_err(|err| Error::with_source("Unable to borrow class", err))?;
        if class_ref.is_abstract() {
            return Err(Error::new("Cannot create an instance of an abstract class"));
        }
        let attributes = class_ref.all_attributes()?;
        Ok(Self {
            notifier: Notifier::new(),
            class,
            container,
            attributes: Self::fill_default(attributes),
            is_valid: false,
        })
    }

    fn fill_default(attributes: Vec<AttributeFeature>) -> HashMap<String, ValueBounded> {
        attributes.into_iter().filter_map(|attribute| {
            if attribute.is_optional() {
                Some((attribute.name, ValueBounded::Option(None)))
            } else if attribute.is_list() {
                Some((attribute.name, ValueBounded::List(Vec::new())))
            } else {
                None
            }
        }).collect()
    }

    /// Check a value against the attribute definition
    /// # Arguments
    /// * `value` - The value to check
    /// * `attribute_feature` - The attribute definition
    /// # Errors
    /// * If the value is not compatible with the attribute definition
    fn validate_value(
        &self,
        value: &Value,
        attribute_feature: &AttributeFeature,
    ) -> Result<(), Error> {
        let result = match value {
            Value::EBoolean(_) => attribute_feature.attribute_type == Type::EBoolean,
            Value::EByte(_) => attribute_feature.attribute_type == Type::EByte,
            Value::EChar(_) => attribute_feature.attribute_type == Type::EChar,
            Value::EDouble(_) => attribute_feature.attribute_type == Type::EDouble,
            Value::EFloat(_) => attribute_feature.attribute_type == Type::EFloat,
            Value::EInt(_) => attribute_feature.attribute_type == Type::EInt,
            Value::ELong(_) => attribute_feature.attribute_type == Type::ELong,
            Value::EShort(_) => attribute_feature.attribute_type == Type::EShort,
            Value::EString(_) => attribute_feature.attribute_type == Type::EString,
            Value::Relation(relation_value) => self
                .validate_relation_value(relation_value, attribute_feature)
                .is_ok(),
        };
        result.then_some(()).ok_or(Error::new(format!(
            "Expected type {}, found {}",
            attribute_feature.attribute_type, value.type_of()?
        )))
    }

    /// Check a relation value against the attribute definition
    /// # Arguments
    /// * `relation_value` - The relation value to check
    /// * `attribute_feature` - The attribute definition
    fn validate_relation_value(
        &self,
        relation_value: &RelationValue,
        attribute_feature: &AttributeFeature,
    ) -> Result<(), Error> {
        let relation = attribute_feature.attribute_type.relation_ref().map_err(|_| {
            Error::new("The attribute is not a relation")
        })?;
        let value_type = relation_value.kind();
        let value_class = relation_value.class()?;

        if !RelationType::is_compatible(&value_type, relation.relation_type()) {
            return Err(Error::new(format!(
                "Kind of reference invalid : expected {}, found {}",
                relation.relation_type(),
                value_type
            )))
        }

        if EClass::is_compatible_with(value_class.clone(), relation.target_type())? {
            Ok(())
        } else {
            Err(Error::new(format!(
                "Invalid reference class : expected {} (or child), found {}",
                EClass::name_with_ref(relation.target_type())?,
                EClass::name_with_ref(value_class)?
            )))
        }
    }

    /// Returns the attribute feature for the given name. Looks for the attribute in the class and super classes of the object
    /// # Arguments
    /// * `name` - The name of the attribute feature.
    /// # Errors
    /// * [`Error`] - If the attribute feature is not found.
    /// * [`Error`] - If the object class is not accessible or unborrowable
    fn attribute_feature(&self, name: &String) -> Result<AttributeFeature, Error> {
        let class_ref = self
            .class
            .upgrade()
            .ok_or_else(|| Error::new("No class or broken reference"))?;
        let class_ref = class_ref
            .try_borrow()
            .map_err(|err| Error::with_source("Unable to borrow class", err))?;
        let attributes = class_ref.all_attributes()?;
        let attribute_feature = attributes
            .iter()
            .find(|attribute_feature| attribute_feature.name == *name)
            .ok_or_else(|| Error::new(format!("Attribute {} not found", name)))?;
        Ok(attribute_feature.clone())
    }

    /// Set the value of an attribute of cardinality 1-1
    /// # Arguments
    /// * `name` - The name of the attribute
    /// * `value` - The value to set
    /// * `should_validate` - If the value should be validated
    /// # Errors
    /// * `Error` - If the attribute is not found or the value is not valid
    pub fn set_unique(
        &mut self,
        name: &str,
        value: Value,
        should_validate: bool,
    ) -> Result<(), Error> {
        let attribute_feature = self.attribute_feature(&name.to_string())?;
        if should_validate {
            self.validate_value(&value, &attribute_feature)?;
        } else {
            self.is_valid = false;
        }
        let value_bounded = ValueBounded::Unique(value);
        let old_value_bound = self.attributes.insert(name.to_string(), value_bounded);
        // SAFETY: unwrap on `get` inserted before
        if let ValueBounded::Unique(new_value) = self.attributes.get(&name.to_string()).unwrap() {
            let old_value = if let Some(ValueBounded::Unique(old_value)) = old_value_bound {
                Some(old_value)
            } else {
                None
            };
            // send notification
            let event = SetEvent {
                old_value,
                new_value,
                position: None,
            };
            let notification = Notification::new(NotificationEvent::Set(event));
            self.notifier
                .notify(self, Some(&attribute_feature), notification);
        } else {
            return Err(Error::new(
                "Error: attribute is not of ValueBounded::Unique type",
            ));
        }
        Ok(())
    }

    /// Unset an optional attribute
    /// # Arguments
    /// * `name` - The name of the attribute to unset
    /// # Errors
    /// * `Error` - If the attribute is not optional
    pub fn unset(&mut self, name: &str) -> Result<(), Error> {
        let attribute_feature = self.attribute_feature(&name.to_string())?;
        if !attribute_feature.is_optional() {
            return Err(Error::new("Can't unset a non optional attribute"));
        }
        let old_bound_opt = self
            .attributes
            .insert(String::from(name), ValueBounded::Option(None));
        let old_value = if let Some(ValueBounded::Option(old_value)) = old_bound_opt {
            old_value
        } else {
            None
        };

        let event = UnsetEvent { old_value };
        let notification = Notification::new(NotificationEvent::Unset(event));
        self.notify(self, Some(&attribute_feature), notification);
        Ok(())
    }

    /// Add an element to a list attribute
    /// # Arguments
    /// * `name` - The name of the attribute to add the element to
    /// * `index` - The index where to add the element
    /// * `element` - The element to add
    /// * `should_validate` - If the value should be validated
    /// # Errors
    /// * `Error` - If the attribute is not a list
    /// * `Error` - If the value is not of the correct type
    /// * `Error` - If the index is out of bounds
    /// * `Error` - If the attribute is not found
    pub fn insert(
        &mut self,
        name: &str,
        index: usize,
        element: Value,
        should_validate: bool,
    ) -> Result<(), Error> {
        let attribute_feature = self.attribute_feature(&name.to_string())?;
        if !attribute_feature.is_list() {
            return Err(Error::new("Can't insert in a non list attribute"));
        }
        if should_validate {
            self.validate_value(&element, &attribute_feature)?;
        } else {
            self.is_valid = false;
        }
        if let Bound::Finite(size) = attribute_feature.upper_bound {
            if index > size {
                return Err(Error::new("Index out of bounds"));
            }
        }
        let value_bounded = self
            .attributes
            .entry(String::from(name))
            .or_insert(ValueBounded::List(Vec::new()));

        if let ValueBounded::List(value) = value_bounded {
            value.insert(index, element);
        } else {
            return Err(Error::new("Attribute is not a list"));
        }

        // get as const version
        let value_bounded_const = self.attributes.get(name).unwrap();
        let value_updated = if let ValueBounded::List(value) = value_bounded_const {
            value
        } else {
            // unreachable : value was checked before, so, fail is not possible
            // we panic if it fail because it's programmation error of this function
            panic!("attribute is not a list")
        };

        if should_validate {
            if let Bound::Finite(size) = attribute_feature.upper_bound {
                if value_updated.len() > size {
                    return Err(Error::new("List size is too small"));
                }
            }
            if value_updated.len() < attribute_feature.lower_bound {
                return Err(Error::new("List size is too small"));
            }
        }
        let event = AddEvent {
            position: index,
            new_value: value_updated.get(index).unwrap(),
        };
        let notification = Notification::new(NotificationEvent::Add(event));
        self.notify(self, Some(&attribute_feature), notification);
        Ok(())
    }

    /// Remove an element from a list attribute at a given index
    /// # Arguments
    /// * `name` - The name of the attribute to remove the element from
    /// * `index` - The index where to remove the element
    /// * `should_validate` - If the value should be validated
    /// # Errors
    /// * `Error` - If the attribute is not a list
    /// * `Error` - If the index is out of bounds
    /// * `Error` - If the attribute is not found
    /// * `Error` - If validation reveals that the list is not of the correct size after the removal
    pub fn remove(&mut self, name: &str, index: usize, should_validate: bool) -> Result<(), Error> {
        let attribute_feature = self.attribute_feature(&name.to_string())?;
        if let Bound::Finite(size) = attribute_feature.upper_bound {
            if index > size {
                return Err(Error::new("Index out of bounds"));
            }
        }
        let value_bounded = self
            .attributes
            .entry(String::from(name))
            .or_insert(ValueBounded::List(Vec::new()));

        if let ValueBounded::List(value) = value_bounded {
            let old_value = value.remove(index);
            if should_validate {
                if let Bound::Finite(size) = attribute_feature.upper_bound {
                    if value.len() > size {
                        return Err(Error::new("List size is too big"));
                    }
                }
                if value.len() < attribute_feature.lower_bound {
                    return Err(Error::new("List size is too small"));
                }
            }
            let event = RemoveEvent {
                position: index,
                old_value,
            };
            let notification = Notification::new(NotificationEvent::Remove(event));
            self.notify(self, Some(&attribute_feature), notification);
        } else {
            return Err(Error::new("Attribute is not a list"));
        }
        Ok(())
    }

    /// Validate all object values against their attribute definition in the class
    /// # Errors
    /// * `Error` - If the object class is not accessible or unborrowable
    /// * `Error` - If the value is not of the correct type
    /// * `Error` - If the value is not in the correct range
    pub fn validate(&mut self) -> Result<(), Error> {
        let class = self
            .class
            .upgrade()
            .ok_or_else(|| Error::new("No class or broken reference"))?;
        let class = class
            .try_borrow()
            .map_err(|err| Error::with_source("Unable to borrow class", err))?;

        let missing:Vec<String> = class.all_attributes()?.into_iter().filter_map(|feature| {
            if self.attributes.contains_key(&feature.name) {
                None
            } else {
                Some(feature.name.clone())
            }
        }).collect();
        if !missing.is_empty() {
            return Err(Error::new(format!("Not all attributes are set: missing {:?}", missing)));
        }
        for (name, value) in self.attributes.iter() {
            let attribute_feature = self.attribute_feature(name)?;
            match value {
                ValueBounded::Unique(value) => {
                    if !attribute_feature.is_unique() {
                        return Err(Error::new(format!("`{}` is not unique", name)));
                    }
                    if let Err(error) = self.validate_value(value, &attribute_feature) {
                        return Err(Error::new(format!("`{}`: {}", name, error)));
                    }
                }
                ValueBounded::Option(value) => {
                    if !attribute_feature.is_optional() {
                        return Err(Error::new(format!("`{}` is not optional", name)));
                    }
                    if let Some(v) = value {
                        if let Err(error) = self.validate_value(v, &attribute_feature) {
                            return Err(Error::new(format!("`{}`: {}", name, error)));
                        }
                    }
                }
                ValueBounded::List(value) => {
                    if !attribute_feature.is_list() {
                        return Err(Error::new(format!("`{}` is not a list", name)));
                    }
                    for (index, v) in value.iter().enumerate() {
                        if let Err(error) = self.validate_value(v, &attribute_feature) {
                            return Err(Error::new(format!("`{}[{}]`: {}", name, index, error)));
                        }
                    }
                }
            }
        }
        self.is_valid = true;
        Ok(())
    }

    /// Return a reference to the object class
    pub fn class(&self) -> EClassRef {
        self.class.clone()
    }

    /// Return the object class name
    /// # Errors
    /// * `Error` - if the class is not set or the reference is broken
    /// * `Error` - if the class is not borrowable
    pub fn class_name(&self) -> Result<String, Error> {
        self.class
            .upgrade()
            .ok_or_else(|| Error::new("No class or broken reference"))
            .and_then(|class_ref| {
                class_ref
                    .try_borrow()
                    .map(|class| class.name().clone())
                    .map_err(|err| Error::with_source("Unable to borrow class", err))
            })
    }

    /// Return the container of the object
    pub fn container(&self) -> RelationReference {
        self.container.clone()
    }

    /// Return the container of the value for a given attribute
    /// # Arguments
    /// * `name` - The attribute name
    /// # Errors
    /// * `Error` - if the attribute is not found
    pub fn get_raw(&self, name: &str) -> Result<&ValueBounded, Error> {
        self.attributes.get(name).ok_or_else(|| {
            let class = self.class_name().unwrap_or_default();
            let msg = format!("No attribute `{}` in class `{}`", name, class);
            Error::new(msg)
        })
    }

    /// Return the value for a given attribute
    /// # Arguments
    /// * `name` - The attribute name
    /// # Errors
    /// * `Error` - if the attribute is not found
    /// * `Error` - if the attribute is a list or unset
    pub fn get<'a, T>(&'a self, name: &str) -> Result<T, Error>
    where
        T: TryFrom<&'a Value, Error = Error>,
    {
        let value_bounded = self.get_raw(name)?;
        match value_bounded {
            ValueBounded::Unique(value) => T::try_from(value),
            ValueBounded::Option(value) => {
                if let Some(value) = value {
                    T::try_from(value)
                } else {
                    Err(Error::new("Value is not set"))
                }
            }
            ValueBounded::List(_value) => {
                Err(Error::new("Multivalued values must be accessed with `get_at`"))
            }
        }
    }

    /// Return the value for a given list attribute at index
    /// # Arguments
    /// * `name` - The attribute name
    /// * `index` - The index in the list
    /// # Errors
    /// * `Error` - if the attribute is not found
    /// * `Error` - if the attribute is not a list
    pub fn get_at<'a, T>(&'a self, name: &str, index: usize) -> Result<T, Error>
    where
        T: TryFrom<&'a Value, Error = Error>,
    {
        let value_bounded = self.get_raw(name)?;
        match value_bounded {
            ValueBounded::List(value) => {
                T::try_from(&value[index])
            }
            _ => Err(Error::new("Value is not a list")),
        }
    }

    /// Return the size of a given list attribute
    /// # Arguments
    /// * `name` - The attribute name
    /// # Errors
    /// * `Error` - if the attribute is not found
    /// * `Error` - if the attribute is not a list
    pub fn size(&self, name: &str) -> Result<usize, Error> {
        let value_bounded = self.get_raw(name)?;
        match value_bounded {
            ValueBounded::List(value) => {
                Ok(value.len())
            }
            _ => Err(Error::new("Value is not a list")),
        }
    }

    /// Return true if the value for a given optional attribute is set
    /// # Arguments
    /// * `name` - The attribute name
    /// # Errors
    /// * `Error` - if the attribute is not found
    /// * `Error` - if the attribute is not optional
    pub fn is_set(&self, name: &str) -> Result<bool, Error> {
        let value_bounded = self.get_raw(name)?;
        match value_bounded {
            ValueBounded::Option(value) => {
                Ok(value.is_some())
            }
            _ => Err(Error::new("Value is not optional")),
        }
    }
}

impl Clone for EObject {
    fn clone(&self) -> Self {
        Self {
            notifier: Notifier::new(),
            class: self.class.clone(),
            container: self.container.clone(),
            attributes: self.attributes.clone(),
            is_valid: self.is_valid,
        }
    }
}

impl Debug for EObject {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let class_printer = RefMutPrinter::new(&self.class, |class, f| {
            f.debug_struct("EClass")
                .field("name", &class.name())
                .finish_non_exhaustive()
        });

        f.debug_struct("EObject")
            .field("class", &class_printer)
            .field("container", &self.container)
            .field("attributes", &self.attributes)
            .finish()
    }
}

impl Display for EObject {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "{} {{",
            self.class_name().unwrap_or_else(|_| String::from("<?>"))
        )?;
        for (name, value) in self.attributes.iter() {
            writeln!(f, "    {}: {};", name, value)?;
        }
        write!(f, "}}")
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use crate::ecore::data_relation::RelationValue;
    use std::rc::{Rc, Weak};
    use std::cell::RefCell;
    use crate::ecore::data_type::Relation;
    use crate::ecore::eclass::test::PetriNet;

    #[test]
    fn test_eobject_car_pilote() {
        //------------------Récuperation des EClass------------------
        //Openings
        let mut opening = EClass::new("Opening".to_string(), true);
        let opening_id = AttributeFeature::new_unique(
            "opening_ID".to_string(),
            false,
            Type::EString);
        //opening.add_attribute(opening_id);
        let ref_opening = Rc::new(RefCell::new(opening));


        //Doors
        let mut door = EClass::new("Door".to_string(),false);
        let door_type = AttributeFeature::new_unique(
            "door_type".to_string(),
            false,
            Type::EString);        //side doors or car trunk
        door.extend_class(Rc::downgrade(&ref_opening)).unwrap();
        door.add_attribute(door_type);
        let ref_door =Rc::new(RefCell::new(door));


        //Windows
        let mut window = EClass::new("Window".to_string(), false);
        let window_type = AttributeFeature::new_unique(
            "window_type".to_string(),
            false,
            Type::EString);        //front, back or side windows
        let window_opacity = AttributeFeature::new_unique(
            "window_opacity".to_string(),
            false,
            Type::EString);    //opaque or transparent
        window.extend_class(Rc::downgrade(&ref_opening)).unwrap();
        window.add_attribute(window_type);
        window.add_attribute(window_opacity);
        let ref_window = Rc::new(RefCell::new(window));


        //Running
        let mut running = EClass::new("Running".to_string(), true);
        let running_id = AttributeFeature::new_unique(
            "running_ID".to_string(),
            false,
            Type::EString);
        running.add_attribute(running_id);
        let ref_running = Rc::new(RefCell::new(running));


        //Wheels
        let mut wheel = EClass::new("Wheel".to_string(), false);
        let wheel_size = AttributeFeature::new_unique(
            "wheel_size".to_string(),
            false,
            Type::EString);
        wheel.extend_class(Rc::downgrade(&ref_running)).unwrap();
        wheel.add_attribute(wheel_size);
        let ref_wheel = Rc::new(RefCell::new(wheel));


        //Color
        let mut color = EClass::new("Color".to_string(), false);
        let color_code = AttributeFeature::new_unique(
            "color_code".to_string(),
            false,
            Type::EString);
        color.add_attribute(color_code);
        let ref_color =Rc::new(RefCell::new(color));


        //Body
        let mut body = EClass::new("Body".to_string(), false);
        let body_color = AttributeFeature::new(
            "body_colors".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_color),
                RelationType::Reference {opposite: None }
        )));
        body.add_attribute(body_color);
        let ref_body = Rc::new(RefCell::new(body));


        //Experience
        let mut experience = EClass::new("Experience".to_string(), false);
        let exp_points = AttributeFeature::new_unique(
            "exp_points".to_string(),
            true,
            Type::EInt);
        experience.add_attribute(exp_points);
        let ref_experience = Rc::new(RefCell::new(experience));


        //Skills
        let mut skill = EClass::new("Skill".to_string(), false);
        let skill_exp = AttributeFeature::new(
            "skill_exp".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_experience),
                RelationType::Reference { opposite: None })));
        skill.add_attribute(skill_exp);
        let ref_skill = Rc::new(RefCell::new(skill));


        //Uniforms
        let mut uniform = EClass::new("Uniform".to_string(), false);
        let uniform_id= AttributeFeature::new_unique(
            "uniform_ID".to_string(),
            false,
            Type::EString);
        uniform.add_attribute(uniform_id);
        let ref_uniform = Rc::new(RefCell::new(uniform));


        //Drivers
        let mut driver = EClass::new("Driver".to_string(), false);
        let first_name = AttributeFeature::new_unique(
            "first_name".to_string(),
            false,
            Type::EString);
        let last_name = AttributeFeature::new_unique(
            "last_name".to_string(),
            false,
            Type::EString);
        let age = AttributeFeature::new_unique(
            "age".to_string(),
            true,
            Type::EInt);
        let driver_uniform = AttributeFeature::new_unique(
            "driver_uniform".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_uniform),
                RelationType::Reference { opposite: None })));
        let driver_skills = AttributeFeature::new_unbounded_list(
            "driver_skills".to_string(),
            true,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_skill),
                RelationType::Reference { opposite: None })));
        driver.add_attribute(first_name);
        driver.add_attribute(last_name);
        driver.add_attribute(age);
        driver.add_attribute(driver_uniform);
        driver.add_attribute(driver_skills);
        let ref_driver = Rc::new(RefCell::new(driver));


        //Sponsors
        let mut sponsor = EClass::new("Sponsor".to_string(), false);
        let company_name = AttributeFeature::new_unique(
            "company_name".to_string(),
            false,
            Type::EString);
        let sponsorship = AttributeFeature::new_unique(
            "sponsorship".to_string(),
            false,
            Type::EInt);
        sponsor.add_attribute(company_name);
        sponsor.add_attribute(sponsorship);
        let ref_sponsor = Rc::new(RefCell::new(sponsor));


        //Cars
        let mut car = EClass::new("Car".to_string(), false);
        let brand = AttributeFeature::new_unique(
            "brand".to_string(),
            false,
            Type::EString);
        let model = AttributeFeature::new_unique(
            "model".to_string(),
            false,
            Type::EString);
        let car_body = AttributeFeature::new_unique(
            "car_body".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_body),
                RelationType::Reference { opposite: None },)));
        let car_wheels = AttributeFeature::new(
            "car_wheels".to_string(),
            true,
            4,
            Bound::Finite(4),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_wheel),
                RelationType::Reference { opposite: None  },)));
        let car_door = AttributeFeature::new(
            "car_door".to_string(),
            true,
            5,
            Bound::Finite(5),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_door),
                RelationType::Reference { opposite: None  },)));
        let car_window = AttributeFeature::new(
            "car_window".to_string(),
            true,
            6,
            Bound::Finite(6),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_window),
                RelationType::Reference { opposite: None  },)));
        let car_sponsor = AttributeFeature::new(
            "car_sponsor".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_sponsor),
                RelationType::Reference { opposite: None },)));
        car.add_attribute(brand);
        car.add_attribute(model);
        car.add_attribute(car_body);
        car.add_attribute(car_wheels);
        car.add_attribute(car_door);
        car.add_attribute(car_window);
        car.add_attribute(car_sponsor);
        let ref_car = Rc::new(RefCell::new(car));
        //bidirectional
        let car_driver = AttributeFeature::new_unique(
            "car_driver".to_string(),
            true,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_driver),
                RelationType::Reference { opposite: Some("driver_car".to_string()) },)));
        let driver_car = AttributeFeature::new_unique(
            "driver_car".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_car),
                RelationType::Reference { opposite: Some("car_driver".to_string()) })));
        ref_driver.borrow_mut().add_attribute(driver_car);
        ref_car.borrow_mut().add_attribute(car_driver);

        //----------------------------------------------------

        //--------------UNIFORM---------------
        //eobject uniform
        let mut eobject_uniform = EObject::new(Rc::downgrade(&ref_uniform), RelationReference::new_invalid()).unwrap();
        eobject_uniform.set_unique("uniform_ID", Value::from("yo"), false).unwrap();
        let eobject_uniform_ref_cell = Rc::new(RefCell::new(eobject_uniform));

        //---------------DRIVER---------------
        //eobject driver
        let mut eobject_driver = EObject::new(Rc::downgrade(&ref_driver), RelationReference::Mutable(Weak::new())).unwrap();
        eobject_driver.set_unique("first_name", Value::from("Thomas"), false).unwrap();
        eobject_driver.set_unique("last_name", Value::from("Georges"), false).unwrap();
        eobject_driver.set_unique("age", Value::from(25), false).unwrap();
        eobject_driver.set_unique("driver_uniform", Value::from(Rc::downgrade(&eobject_uniform_ref_cell)), false).unwrap();
        let driver_ref_cell = Rc::new(RefCell::new(eobject_driver));

        //---------------CAR---------------
        //eobject car
        let mut eobject_car = EObject::new(Rc::downgrade(&ref_car), RelationReference::Mutable(Weak::new())).unwrap();
        eobject_car.set_unique("car_driver", Value::Relation(RelationValue::Reference(RelationReference::Mutable(Rc::downgrade(&driver_ref_cell)))), false).unwrap();
        let car_ref_cell = Rc::new(RefCell::new(eobject_car));
        car_ref_cell.borrow_mut().set_unique("car_driver", Value::from(Rc::downgrade(&driver_ref_cell)), false).unwrap();
        car_ref_cell.borrow_mut().set_unique("brand", Value::from("Toyota"), false).unwrap();
        car_ref_cell.borrow_mut().set_unique("model", Value::from("Supra"), false).unwrap();

        driver_ref_cell.borrow_mut().set_unique("driver_car", Value::from(Rc::downgrade(&car_ref_cell)), false).unwrap();


        //---------------WINDOW---------------
        //eobject window windshield
        let mut eobject_window_windshield= EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_windshield.set_unique("window_type", Value::from("windshield"), false).unwrap();
        let window_windshield_ref_cell = Rc::new(RefCell::new(eobject_window_windshield));
        window_windshield_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 0, Value::from(Rc::downgrade(&window_windshield_ref_cell)), false).unwrap();

        //eobject window front left
        let mut eobject_window_front_left = EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_front_left.set_unique("window_type", Value::from("front_left"), false).unwrap();
        let window_front_left_ref_cell = Rc::new(RefCell::new(eobject_window_front_left));
        window_front_left_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 1, Value::from(Rc::downgrade(&window_front_left_ref_cell)), false).unwrap();

        //eobject window front right
        let mut eobject_window_front_right = EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_front_right.set_unique("window_type", Value::from("front_left"), false).unwrap();
        let window_front_right_ref_cell = Rc::new(RefCell::new(eobject_window_front_right));
        window_front_right_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 2, Value::from(Rc::downgrade(&window_front_right_ref_cell)), false).unwrap();

        //eobject window back left
        let mut eobject_window_back_left = EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_back_left.set_unique("window_type", Value::from("front_left"), false).unwrap();
        let window_back_left_ref_cell = Rc::new(RefCell::new(eobject_window_back_left));
        window_back_left_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 3, Value::from(Rc::downgrade(&window_back_left_ref_cell)), false).unwrap();

        //eobject window back right
        let mut eobject_window_back_right = EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_back_right.set_unique("window_type", Value::from("front_left"), false).unwrap();
        let window_back_right_ref_cell = Rc::new(RefCell::new(eobject_window_back_right));
        window_back_right_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 4, Value::from(Rc::downgrade(&window_back_right_ref_cell)), false).unwrap();

        //eobject window back
        let mut eobject_window_back = EObject::new(Rc::downgrade(&ref_window), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_window_back.set_unique("window_type", Value::from("back"), false).unwrap();
        let window_windshield_back_ref_cell = Rc::new(RefCell::new(eobject_window_back));
        window_windshield_back_ref_cell.borrow_mut().set_unique("window_opacity", Value::from("simple"), false).unwrap();
        car_ref_cell.borrow_mut().insert("car_window", 5, Value::from(Rc::downgrade(&window_windshield_back_ref_cell)), false).unwrap();

        //---------------SPONSOR---------------
        //eobject sponsor
        let mut eobject_sponsor= EObject::new(Rc::downgrade(&ref_sponsor), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_sponsor.set_unique("company_name", Value::from("LIDL"), false).unwrap();
        eobject_sponsor.set_unique("sponsorship", Value::from(12), false).unwrap();
        let sponsor_ref_cell = Rc::new(RefCell::new(eobject_sponsor));
        car_ref_cell.borrow_mut().insert("car_sponsor", 0, Value::from(Rc::downgrade(&sponsor_ref_cell)), false).unwrap();

        //---------------WHEEL---------------
        //eobject wheel front left
        let mut eobject_wheel_front_left= EObject::new(Rc::downgrade(&ref_wheel), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_wheel_front_left.set_unique("wheel_size", Value::from("21"), false).unwrap();
        eobject_wheel_front_left.set_unique("running_ID", Value::from("1"), false).unwrap();
        let wheel_front_left_ref_cell = Rc::new(RefCell::new(eobject_wheel_front_left));
        car_ref_cell.borrow_mut().insert("car_wheels", 0,Value::from(Rc::downgrade(&wheel_front_left_ref_cell)), false).unwrap();

        //eobject wheel front right
        let mut eobject_wheel_front_right= EObject::new(Rc::downgrade(&ref_wheel), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_wheel_front_right.set_unique("wheel_size", Value::from("21"), false).unwrap();
        eobject_wheel_front_right.set_unique("running_ID", Value::from("2"), false).unwrap();
        let wheel_front_right_ref_cell = Rc::new(RefCell::new(eobject_wheel_front_right));
        car_ref_cell.borrow_mut().insert("car_wheels", 1,Value::from(Rc::downgrade(&wheel_front_right_ref_cell)), false).unwrap();

        //eobject wheel back left
        let mut eobject_wheel_back_left= EObject::new(Rc::downgrade(&ref_wheel), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_wheel_back_left.set_unique("wheel_size", Value::from("19"), false).unwrap();
        eobject_wheel_back_left.set_unique("running_ID", Value::from("3"), false).unwrap();
        let wheel_back_left_ref_cell = Rc::new(RefCell::new(eobject_wheel_back_left));
        car_ref_cell.borrow_mut().insert("car_wheels", 2,Value::from(Rc::downgrade(&wheel_back_left_ref_cell)), false).unwrap();

        //eobject wheel back right
        let mut eobject_wheel_back_right= EObject::new(Rc::downgrade(&ref_wheel), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_wheel_back_right.set_unique("wheel_size", Value::from("19"), false).unwrap();
        eobject_wheel_back_right.set_unique("running_ID", Value::from("4"), false).unwrap();
        let wheel_back_right_ref_cell = Rc::new(RefCell::new(eobject_wheel_back_right));
        car_ref_cell.borrow_mut().insert("car_wheels", 3,Value::from(Rc::downgrade(&wheel_back_right_ref_cell)), false).unwrap();

        //---------------DOOR---------------
        //eobject door front left
        let mut eobject_door_front_left= EObject::new(Rc::downgrade(&ref_door), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_door_front_left.set_unique("door_type", Value::from("classical"), false).unwrap();
        let door_door_front_left_ref_cell = Rc::new(RefCell::new(eobject_door_front_left));
        car_ref_cell.borrow_mut().insert("car_door", 0, Value::from(Rc::downgrade(&door_door_front_left_ref_cell)), false).unwrap();

        //eobject door front right
        let mut eobject_door_front_right= EObject::new(Rc::downgrade(&ref_door), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_door_front_right.set_unique("door_type", Value::from("classical"), false).unwrap();
        let door_door_front_right_ref_cell = Rc::new(RefCell::new(eobject_door_front_right));
        car_ref_cell.borrow_mut().insert("car_door", 1, Value::from(Rc::downgrade(&door_door_front_right_ref_cell)), false).unwrap();

        //eobject door back left
        let mut eobject_door_back_left= EObject::new(Rc::downgrade(&ref_door), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_door_back_left.set_unique("door_type", Value::from("classical"), false).unwrap();
        let door_door_back_left_ref_cell = Rc::new(RefCell::new(eobject_door_back_left));
        car_ref_cell.borrow_mut().insert("car_door", 2, Value::from(Rc::downgrade(&door_door_back_left_ref_cell)), false).unwrap();

        //eobject door back right
        let mut eobject_door_back_right= EObject::new(Rc::downgrade(&ref_door), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_door_back_right.set_unique("door_type", Value::from("classical"), false).unwrap();
        let door_door_back_right_ref_cell = Rc::new(RefCell::new(eobject_door_back_right));
        car_ref_cell.borrow_mut().insert("car_door", 3, Value::from(Rc::downgrade(&door_door_back_right_ref_cell)), false).unwrap();

        //eobject door back
        let mut eobject_door_back= EObject::new(Rc::downgrade(&ref_door), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        eobject_door_back.set_unique("door_type", Value::from("classical"), false).unwrap();
        let door_door_back_ref_cell = Rc::new(RefCell::new(eobject_door_back));
        car_ref_cell.borrow_mut().insert("car_door", 4, Value::from(Rc::downgrade(&door_door_back_ref_cell)), false).unwrap();

        //---------------BODY---------------
        //eobject body
        let mut eobject_body= EObject::new(Rc::downgrade(&ref_body), RelationReference::Mutable(Rc::downgrade(&car_ref_cell))).unwrap();
        let body_ref_cell = Rc::new(RefCell::new(eobject_body));
        car_ref_cell.borrow_mut().set_unique("car_body", Value::from(Rc::downgrade(&body_ref_cell)), false).unwrap();

        //---------------COLOR BODY---------------
        //eobject color body
        let mut eobject_color_body= EObject::new(Rc::downgrade(&ref_color), RelationReference::Mutable(Rc::downgrade(&body_ref_cell))).unwrap();
        eobject_color_body.set_unique("color_code", Value::from("245 43 133"), false).unwrap();
        let color_body_ref_cell = Rc::new(RefCell::new(eobject_color_body));
        body_ref_cell.borrow_mut().insert("body_colors", 0,Value::from(Rc::downgrade(&color_body_ref_cell)), false).unwrap();

        //---------------COLOR---------------
        //eobject color
        let mut eobject_color= EObject::new(Rc::downgrade(&ref_color), RelationReference::Mutable(Rc::downgrade(&body_ref_cell))).unwrap();
        eobject_color.set_unique("color_code", Value::from("245 43 133"), false).unwrap();
        let color_ref_cell = Rc::new(RefCell::new(eobject_color));

        //---------------UNIFORM---------------
        //eobject uniform
        let mut eobject_uniform= EObject::new(Rc::downgrade(&ref_uniform), RelationReference::Mutable(Rc::downgrade(&driver_ref_cell))).unwrap();
        eobject_uniform.set_unique("uniform_ID", Value::from("Yellow and Blue of RedBull"), false).unwrap();
        let uniform_ref_cell = Rc::new(RefCell::new(eobject_uniform));

        //---------------EXPERIENCE---------------
        //eobject experience_1
        let mut eobject_experience_1= EObject::new(Rc::downgrade(&ref_experience), RelationReference::Mutable(Rc::downgrade(&driver_ref_cell))).unwrap();
        eobject_experience_1.set_unique("exp_points", Value::from(100), false).unwrap();
        let experience_1_ref_cell = Rc::new(RefCell::new(eobject_experience_1));

        //eobject experience_2
        let mut eobject_experience_2= EObject::new(Rc::downgrade(&ref_experience), RelationReference::Mutable(Rc::downgrade(&driver_ref_cell))).unwrap();
        eobject_experience_2.set_unique("exp_points", Value::from(200), false).unwrap();
        let experience_2_ref_cell = Rc::new(RefCell::new(eobject_experience_2));

        //---------------SKILL---------------
        //eobject skill
        let mut eobject_skill= EObject::new(Rc::downgrade(&ref_skill), RelationReference::Mutable(Rc::downgrade(&driver_ref_cell))).unwrap();
        eobject_skill.insert("skill_exp", 0, Value::from(Rc::downgrade(&experience_1_ref_cell)), false).unwrap();
        let skill_ref_cell = Rc::new(RefCell::new(eobject_skill));
        driver_ref_cell.borrow_mut().insert("driver_skills", 0,Value::from(Rc::downgrade(&skill_ref_cell)), false).unwrap(); 
    

       
        //---------------ABSTRACT INSTANTIATION ERROR---------------
        EObject::new(Rc::downgrade(&ref_opening), RelationReference::Mutable(Weak::new())).unwrap_err();

        //---------------VALIDATION---------------
        eobject_uniform_ref_cell.borrow_mut().validate().unwrap();
        car_ref_cell.borrow_mut().validate().unwrap();
        driver_ref_cell.borrow_mut().validate().unwrap();
        window_windshield_ref_cell.borrow_mut().validate().unwrap();
        window_back_left_ref_cell.borrow_mut().validate().unwrap();
        window_back_right_ref_cell.borrow_mut().validate().unwrap();
        window_front_left_ref_cell.borrow_mut().validate().unwrap();
        window_front_right_ref_cell.borrow_mut().validate().unwrap();
        sponsor_ref_cell.borrow_mut().validate().unwrap();
        wheel_back_left_ref_cell.borrow_mut().validate().unwrap();
        wheel_back_right_ref_cell.borrow_mut().validate().unwrap();
        wheel_front_left_ref_cell.borrow_mut().validate().unwrap();
        wheel_front_right_ref_cell.borrow_mut().validate().unwrap();
        door_door_back_left_ref_cell.borrow_mut().validate().unwrap();
        door_door_back_ref_cell.borrow_mut().validate().unwrap();
        door_door_back_right_ref_cell.borrow_mut().validate().unwrap();
        door_door_front_left_ref_cell.borrow_mut().validate().unwrap();
        door_door_front_right_ref_cell.borrow_mut().validate().unwrap();
        body_ref_cell.borrow_mut().validate().unwrap();
        color_ref_cell.borrow_mut().validate().unwrap();
        color_body_ref_cell.borrow_mut().validate().unwrap();
        uniform_ref_cell.borrow_mut().validate().unwrap();
        skill_ref_cell.borrow_mut().validate().unwrap();
        experience_1_ref_cell.borrow_mut().validate().unwrap();
        experience_2_ref_cell.borrow_mut().validate().unwrap();


    }

    #[test]
    fn test_eobject_petrinet() {
        let pkg = PetriNet::new();
        //pkg.petrinet_ref()

        let petri_test = EObject::new(pkg.petrinet_ref(), RelationReference::new_invalid()).unwrap();
        let petri_test = Rc::new(RefCell::new(petri_test));

        //-------------ORIENTATION-------------
        let mut orientation_transition = EObject::new(pkg.orientation_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        orientation_transition.set_unique("is_transition", Value::from(true), false).unwrap();
        let orientation_transition_ref_cell = Rc::new(RefCell::new(orientation_transition));
        let mut orientation_place = EObject::new(pkg.orientation_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        orientation_place.set_unique("is_transition", Value::from(false), false).unwrap();
        let orientation_place_ref_cell = Rc::new(RefCell::new(orientation_place));

        //-------------PLACE-------------
        let mut place1 = EObject::new(pkg.place_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        place1.set_unique("name", Value::from("P1"), false).unwrap();
        place1.set_unique("tokens", Value::from(1), false).unwrap();
        let place1_ref_cell = Rc::new(RefCell::new(place1));

        let mut place2 = EObject::new(pkg.place_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        place2.set_unique("name", Value::from("P2"), false).unwrap();
        place2.set_unique("tokens", Value::from(0), false).unwrap();
        let place2_ref_cell = Rc::new(RefCell::new(place2));

        let mut place3 = EObject::new(pkg.place_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        place3.set_unique("name", Value::from("P3"), false).unwrap();
        place3.set_unique("tokens", Value::from(2), false).unwrap();
        let place3_ref_cell = Rc::new(RefCell::new(place3));

        let mut place4 = EObject::new(pkg.place_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        place4.set_unique("name", Value::from("P4"), false).unwrap();
        place4.set_unique("tokens", Value::from(1), false).unwrap();
        let place4_ref_cell = Rc::new(RefCell::new(place4));


        //-------------EDGE-------------
        let mut edge1 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge1.set_unique("weight", Value::from("E1"), false).unwrap();
        edge1.set_unique("orientation", Value::from(Rc::downgrade(&orientation_transition_ref_cell)), false).unwrap();
        let edge1_ref_cell = Rc::new(RefCell::new(edge1));

        let mut edge2 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge2.set_unique("weight", Value::from("E2"), false).unwrap();
        edge2.set_unique("orientation", Value::from(Rc::downgrade(&orientation_place_ref_cell)), false).unwrap();
        let edge2_ref_cell = Rc::new(RefCell::new(edge2));

        let mut edge3 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge3.set_unique("weight", Value::from("E3"), false).unwrap();
        edge3.set_unique("orientation", Value::from(Rc::downgrade(&orientation_place_ref_cell)), false).unwrap();
        let edge3_ref_cell = Rc::new(RefCell::new(edge3));

        let mut edge4 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge4.set_unique("weight", Value::from("E4"), false).unwrap();
        edge4.set_unique("orientation", Value::from(Rc::downgrade(&orientation_transition_ref_cell)), false).unwrap();
        let edge4_ref_cell = Rc::new(RefCell::new(edge4));

        let mut edge5 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge5.set_unique("weight", Value::from("E5"), false).unwrap();
        edge5.set_unique("orientation", Value::from(Rc::downgrade(&orientation_transition_ref_cell)), false).unwrap();
        let edge5_ref_cell = Rc::new(RefCell::new(edge5));

        let mut edge6 = EObject::new(pkg.edge_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        edge6.set_unique("weight", Value::from("E6"), false).unwrap();
        edge6.set_unique("orientation", Value::from(Rc::downgrade(&orientation_place_ref_cell)), false).unwrap();
        let edge6_ref_cell = Rc::new(RefCell::new(edge6));

        //-------------TRANSITION-------------
        let mut transition1 = EObject::new(pkg.transition_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        transition1.set_unique("name", Value::from("T1"), false).unwrap();
        let transition1_ref_cell= Rc::new(RefCell::new(transition1));

        let mut transition2 = EObject::new(pkg.transition_ref(), RelationReference::from(Rc::downgrade(&petri_test))).unwrap();
        transition2.set_unique("name", Value::from("T2"), false).unwrap();
        let transition2_ref_cell= Rc::new(RefCell::new(transition2));

        //-------------PLACE RELATION-------------
        //input_edges
        place2_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge2_ref_cell)), false).unwrap();
        place3_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge3_ref_cell)), false).unwrap();
        place4_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge6_ref_cell)), false).unwrap();

        //output_edges
        place1_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge1_ref_cell)), false).unwrap();
        place2_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge4_ref_cell)), false).unwrap();
        place3_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge5_ref_cell)), false).unwrap();

        //-------------EDGE RELATION-------------
        //place
        edge1_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place1_ref_cell)), false).unwrap();
        edge2_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place2_ref_cell)), false).unwrap();
        edge3_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place3_ref_cell)), false).unwrap();
        edge4_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place2_ref_cell)), false).unwrap();
        edge5_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place3_ref_cell)), false).unwrap();
        edge6_ref_cell.borrow_mut().set_unique("place", Value::from(Rc::downgrade(&place4_ref_cell)), false).unwrap();

        //transition
        edge1_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition1_ref_cell)), false).unwrap();
        edge2_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition1_ref_cell)), false).unwrap();
        edge3_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition1_ref_cell)), false).unwrap();
        edge4_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition2_ref_cell)), false).unwrap();
        edge5_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition2_ref_cell)), false).unwrap();
        edge6_ref_cell.borrow_mut().set_unique("transition", Value::from(Rc::downgrade(&transition2_ref_cell)), false).unwrap();

        //-------------TRANSITION RELATION-------------
        //input_edges
        transition1_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge1_ref_cell)), false).unwrap();
        transition2_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge4_ref_cell)), false).unwrap();
        transition2_ref_cell.borrow_mut().insert("input_edges", 0, Value::from(Rc::downgrade(&edge5_ref_cell)), false).unwrap();

        //output_edges
        transition1_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge3_ref_cell)), false).unwrap();
        transition1_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge2_ref_cell)), false).unwrap();
        transition2_ref_cell.borrow_mut().insert("output_edges", 0, Value::from(Rc::downgrade(&edge6_ref_cell)), false).unwrap();

        //-------------VALIDATE-------------
        place1_ref_cell.borrow_mut().validate().unwrap();
        place2_ref_cell.borrow_mut().validate().unwrap();
        place3_ref_cell.borrow_mut().validate().unwrap();
        place4_ref_cell.borrow_mut().validate().unwrap();
        edge1_ref_cell.borrow_mut().validate().unwrap();
        edge2_ref_cell.borrow_mut().validate().unwrap();
        edge3_ref_cell.borrow_mut().validate().unwrap();
        edge4_ref_cell.borrow_mut().validate().unwrap();
        edge5_ref_cell.borrow_mut().validate().unwrap();
        edge6_ref_cell.borrow_mut().validate().unwrap();
        transition1_ref_cell.borrow_mut().validate().unwrap();
        transition2_ref_cell.borrow_mut().validate().unwrap();

    }

}
