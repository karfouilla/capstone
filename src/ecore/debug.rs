use std::cell::RefCell;
use std::fmt::{Debug, Display, Formatter};
use std::ops::Deref;
use std::rc::{Rc, Weak};

// create adapter struct for EClass debug print
// tip from Debug trait for RefCell (in src/core/fmt/mod.rs#2618)
pub struct UnreachablePlaceholder;
pub struct BorrowedPlaceholder;

struct ClosurePrinter<'a, T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	data: &'a T,
	format_func: &'a F,
}

pub struct RefPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	data: Weak<T>,
	format_func: F,
}

pub struct RefMutPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	data: Weak<RefCell<T>>,
	format_func: F,
}

// UnreachablePlaceholder
impl Debug for UnreachablePlaceholder {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "<unreachable>")
	}
}

impl Display for UnreachablePlaceholder {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "<unreachable>")
	}
}

// BorrowedPlaceholder
impl Debug for BorrowedPlaceholder {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		// make dummy borrowed RefCell to print debug <borrowed>
		let dummy = Rc::new(RefCell::new(()));
		let _ref_mut = dummy.borrow_mut();
		std::fmt::Debug::fmt(&dummy, f)
	}
}

impl Display for BorrowedPlaceholder {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "<borrowed>")
	}
}

// ClosurePrinter
impl<T, F> Debug for ClosurePrinter<'_, T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		(self.format_func)(self.data, f)
	}
}

// WeakPrinter
impl<T, F> RefPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	pub fn new(data: &Weak<T>, format_func: F) -> Self {
		Self {
			data: data.clone(),
			format_func,
		}
	}
}

impl<T, F> Debug for RefPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		if let Some(object) = self.data.upgrade() {
			let object_debug = ClosurePrinter {
				data: object.deref(),
				format_func: &self.format_func,
			};
			f.debug_struct("Weak")
				.field("value", &object_debug)
				.finish()
		} else {
			f.debug_struct("Weak")
				.field("value", &UnreachablePlaceholder)
				.finish()
		}
	}
}

impl<T, F> Display for RefPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		if let Some(object) = self.data.upgrade() {
			(self.format_func)(&object, f)
		} else {
			write!(f, "{}", &UnreachablePlaceholder)
		}
	}
}

// WeakRefCellPrinter
impl<T, F> RefMutPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	pub fn new(data: &Weak<RefCell<T>>, format_func: F) -> Self {
		Self {
			data: data.clone(),
			format_func,
		}
	}
}

impl<T, F> Debug for RefMutPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		Debug::fmt(
			&RefPrinter::new(&self.data, |this, f| {
				if let Ok(object) = this.try_borrow() {
					Debug::fmt(
						&RefCell::new(ClosurePrinter {
							data: object.deref(),
							format_func: &self.format_func,
						}),
						f,
					)
				} else {
					Debug::fmt(&BorrowedPlaceholder, f)
				}
			}),
			f,
		)
	}
}

impl<T, F> Display for RefMutPrinter<T, F>
where
	F: Fn(&T, &mut Formatter<'_>) -> std::fmt::Result,
{
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		Display::fmt(
			&RefPrinter::new(&self.data, |this, f| {
				if let Ok(object) = this.try_borrow() {
					(self.format_func)(&object, f)
				} else {
					Display::fmt(&BorrowedPlaceholder, f)
				}
			}),
			f,
		)
	}
}
