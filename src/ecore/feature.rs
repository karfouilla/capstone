use crate::ecore::data_type::Type;
use std::fmt::{Display, Formatter};

/// A feature of an [`EClass`]. This can be an primitive type or a reference.
/// The feature can be mutable or not.
/// The feature can be unique, i.e. it can only have one value, or it can be a list.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct AttributeFeature {
    pub name: String,
    pub mutable: bool,
    pub lower_bound: usize,
    pub upper_bound: Bound,
    pub attribute_type: Type,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Bound {
    Unbounded,
    Finite(usize),
}

impl Display for Bound {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Bound::Unbounded => write!(f, "*"),
            Bound::Finite(finite) => write!(f, "{}", finite),
        }
    }
}

impl AttributeFeature {
    /// Create a new attribute feature
    /// # Arguments
    /// * `name` - The name of the attribute
    /// * `mutable` - True if the attribute is mutable
    /// * `lower_bound` - The lower bound of the attribute
    /// * `upper_bound` - The upper bound of the attribute
    /// * `attribute_type` - The type of the attribute
    pub fn new(
        name: String,
        mutable: bool,
        lower_bound: usize,
        upper_bound: Bound,
        attribute_type: Type,
    ) -> Self {
        Self {
            name,
            mutable,
            lower_bound,
            upper_bound,
            attribute_type,
        }
    }

    /// Create a new unique type attribute feature. Bounds are set to 1-1.
    /// # Arguments
    /// * `name` - The name of the attribute
    /// * `mutable` - True if the attribute is mutable
    /// * `attribute_type` - The type of the attribute
    pub fn new_unique(name: String, mutable: bool, attribute_type: Type) -> Self {
        Self {
            name,
            mutable,
            lower_bound: 1,
            upper_bound: Bound::Finite(1),
            attribute_type,
        }
    }

    /// Create a new optional type attribute feature. Bounds are set to 0-1.
    /// # Arguments
    /// * `name` - The name of the attribute
    /// * `mutable` - True if the attribute is mutable
    /// * `attribute_type` - The type of the attribute
    pub fn new_optional(name: String, mutable: bool, attribute_type: Type) -> Self {
        Self {
            name,
            mutable,
            lower_bound: 0,
            upper_bound: Bound::Finite(1),
            attribute_type,
        }
    }

    /// Create a new unbounded type attribute feature. Bounds are set to 0-*.
    /// # Arguments
    /// * `name` - The name of the attribute
    /// * `mutable` - True if the attribute is mutable
    /// * `attribute_type` - The type of the attribute
    pub fn new_unbounded_list(name: String, mutable: bool, attribute_type: Type) -> Self {
        Self {
            name,
            mutable,
            lower_bound: 0,
            upper_bound: Bound::Unbounded,
            attribute_type,
        }
    }

    /// Returns true if the attribute is a single value, i.e. it has a lower bound of 1 and an upper bound of 1.
    pub fn is_unique(&self) -> bool {
        self.lower_bound == 1 && self.upper_bound == Bound::Finite(1)
    }

    /// Returns true if the attribute is a list, i.e. it has a upper bound greater than 1.
    pub fn is_list(&self) -> bool {
        match self.upper_bound {
            Bound::Unbounded => true,
            Bound::Finite(upper_bound) => upper_bound > 1,
        }
    }

    /// Returns true if the attribute is optional, i.e. it has a lower bound of 0 and an upper bound of 1.
    pub fn is_optional(&self) -> bool {
        self.lower_bound == 0 && self.upper_bound == Bound::Finite(1)
    }
}

impl Display for AttributeFeature {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let name = &self.name;
        let mutable = if self.mutable { "mut " } else { "" };
        let ty = &self.attribute_type;
        let lower = self.lower_bound;
        let upper = self.upper_bound.clone();
        write!(f, "{}: {}{}[{},{}]", name, mutable, ty, lower, upper)
    }
}

/*
#[cfg(test)]
mod test {
    use super::*;
    use crate::ecore::eclass::test::generate_user_eclass;
    use std::rc::Rc;

    #[test]
    fn test_attribute_new() {
        let attr_const = AttributeFeature::new_const("name", Type::EString);
        let attr_mut = AttributeFeature::new_mut("name", Type::EString);
        let attr_list = AttributeFeature::new_list("names", Type::EString);

        let attr_const_new = AttributeFeature {
            name: String::from("name"),
            mutable: false,
            lower_bound: Bound::Finite(1),
            upper_bound: Bound::Finite(1),
            attr_type: Type::EString,
        };
        let attr_mut_new = AttributeFeature {
            name: String::from("name"),
            mutable: true,
            lower_bound: Bound::Finite(1),
            upper_bound: Bound::Finite(1),
            attr_type: Type::EString,
        };
        let attr_list_new = AttributeFeature {
            name: String::from("names"),
            mutable: true,
            lower_bound: Bound::Finite(0),
            upper_bound: Bound::Unbounded,
            attr_type: Type::EString,
        };

        assert_eq!(attr_const, attr_const_new);
        assert_eq!(attr_mut, attr_mut_new);
        assert_eq!(attr_list, attr_list_new);
    }

    #[test]
    fn test_attribute_ref() {
        let user_eclass = generate_user_eclass();
        let owner = Type::new_owner(Rc::downgrade(&user_eclass));
        let ty_ref = Type::new_ref(Rc::downgrade(&user_eclass));

        let attr_object = AttributeFeature::new_const("user", owner.clone());
        let attr_ref = AttributeFeature::new_const("user", ty_ref.clone());
        let attr_ref_list = AttributeFeature::new_list("users", ty_ref.clone());

        let attr_object_new = AttributeFeature {
            name: String::from("user"),
            mutable: false,
            lower_bound: Bound::Finite(1),
            upper_bound: Bound::Finite(1),
            attr_type: owner.clone(),
        };
        let attr_ref_new = AttributeFeature {
            name: String::from("user"),
            mutable: false,
            lower_bound: Bound::Finite(1),
            upper_bound: Bound::Finite(1),
            attr_type: ty_ref.clone(),
        };
        let attr_ref_list_new = AttributeFeature {
            name: String::from("users"),
            mutable: true,
            lower_bound: Bound::Finite(0),
            upper_bound: Bound::Unbounded,
            attr_type: ty_ref.clone(),
        };

        assert_eq!(attr_object, attr_object_new);
        assert_eq!(attr_ref, attr_ref_new);
        assert_eq!(attr_ref_list, attr_ref_list_new);
    }

    #[test]
    fn test_attribute_display() {
        let user_eclass = generate_user_eclass();
        let owner = Type::new_owner(Rc::downgrade(&user_eclass));
        let ty_ref = Type::new_ref(Rc::downgrade(&user_eclass));

        let attr_const = AttributeFeature::new_const("name", Type::EString);
        let attr_mut = AttributeFeature::new_mut("name", Type::EString);
        let attr_list = AttributeFeature::new_list("names", Type::EString);
        let attr_object = AttributeFeature::new_const("user", owner.clone());
        let attr_ref = AttributeFeature::new_const("user", ty_ref.clone());
        let attr_ref_list = AttributeFeature::new_list("users", ty_ref.clone());

        assert_eq!(format!("{}", attr_const), "name: String[1,1]");
        assert_eq!(format!("{}", attr_mut), "name: mut String[1,1]");
        assert_eq!(format!("{}", attr_list), "names: mut String[0,*]");

        assert_eq!(format!("{}", attr_object), "user: User(Owner)[1,1]");
        assert_eq!(format!("{}", attr_ref), "user: User(Reference)[1,1]");
        assert_eq!(
            format!("{}", attr_ref_list),
            "users: mut User(Reference)[0,*]"
        );
    }
}
 */
