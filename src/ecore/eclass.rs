use crate::ecore::feature::AttributeFeature;
use std::cell::RefCell;
use std::fmt::{Debug, Display, Formatter};
use std::rc::{Rc, Weak};

use super::error::Error;

/// An EClass is a class of objects in the metamodel.
/// It is a collection of [`AttributeFeature`], which are either primitive types or references.
/// It can also have super classes, which are other EClasses.
/// An EClass can be abstract, which means that it cannot be instantiated.
#[derive(Debug, Clone)]
pub struct EClass {
    name: String,
    attributes: Vec<AttributeFeature>,
    super_classes: Vec<EClassRef>,
    is_abstract: bool,
}

pub type EClassRef = Weak<RefCell<EClass>>;

// Implementations
impl EClass {
    /// Creates a new EClass with the given name and
    pub fn new(name: String, is_abstract: bool) -> Self {
        Self {
            name,
            attributes: Vec::new(),
            super_classes: Vec::new(),
            is_abstract,
        }
    }

    /// Add the given attribute to the class.
    pub fn add_attribute(&mut self, attribute: AttributeFeature) {
        self.attributes.push(attribute);
    }

    /// Removes the given attribute from the class.
    pub fn remove_attribute(&mut self, attribute: &AttributeFeature) {
        self.attributes.retain(|attr| attr != attribute);
    }

    /// Returns all attributes of this class.
    pub fn attributes(&self) -> &Vec<AttributeFeature> {
        &self.attributes
    }

	/// Returns all attributes of this class and all its super classes.
	/// Attributes are returned in the inverse order of the inheritance hierarchy.
	/// # Errors
	/// * `Error` - If a super class cannot be upgraded.
	/// * `Error` - If a super class cannot be borrowed.
	pub fn all_attributes(&self) -> Result<Vec<AttributeFeature>, Error> {
		let mut stack = self.super_classes.iter().map(|super_class| {
            super_class
                .upgrade()
                .ok_or_else(|| Error::new("Unable to upgrade class"))
        }).collect::<Result<Vec<Rc<RefCell<EClass>>>, Error>>()?;
        let mut attributes: Vec<AttributeFeature> = self.attributes.clone();
        while let Some(current) = stack.pop() {
            let current = current
                .try_borrow()
                .map_err(|err| Error::with_source("Unable to borrow class", err))?;
            for attribute in &current.attributes {
                // Skip attributes that are already in the list. Allows overriding of attributes.
                if !attributes.iter().any(|a| a.name == attribute.name) {
                    attributes.push(attribute.clone());
                }
            }
			for super_class in current.super_classes.iter() {
				let super_class = super_class
					.upgrade()
					.ok_or_else(|| Error::new("Unable to upgrade class"))?;
				stack.push(super_class);
			}
		}
		Ok(attributes)
	}

    /// Returns all inherited types of this class.
    pub fn inherited_types(&self) -> Result<Vec<EClassRef>, Error> {
        let mut parents: Vec<EClassRef> = Vec::new();
        let mut stack: Vec<Rc<RefCell<EClass>>> = Vec::new();
        for super_class in &self.super_classes {
            parents.push(super_class.clone());
            let super_class = super_class
                .upgrade()
                .ok_or_else(|| Error::new("Unable to upgrade class"))?;
            stack.push(super_class);
        }
        while let Some(current) = stack.pop() {
            let current = current
                .try_borrow()
                .map_err(|err| Error::with_source("Unable to borrow class", err))?;
            for super_class in &current.super_classes {
                parents.push(super_class.clone());
                let super_class = super_class
                    .upgrade()
                    .ok_or_else(|| Error::new("Unable to upgrade class"))?;
                stack.push(super_class);
            }
        }
        Ok(parents)
    }

    /// Return if `class` is child of `reference` or is same that reference
    pub fn is_compatible_with(class: EClassRef, reference: EClassRef) -> Result<bool, Error> {
        if class.ptr_eq(&reference) {
            Ok(true)
        } else {
            let parents = class
                .upgrade()
                .ok_or(Error::new("Unable to upgrade class"))?
                .try_borrow_mut()
                .map_err(|err| Error::with_source("Unable to borrow class", err))?
                .inherited_types()?;
            Ok(parents.into_iter().any(|parent| class.ptr_eq(&parent)))
        }
    }

    pub fn name_with_ref(class_ref: EClassRef) -> Result<String, Error> {
        let name = class_ref
            .upgrade()
            .ok_or(Error::new("Unable to upgrade class"))?
            .try_borrow_mut()
            .map_err(|err| Error::with_source("Unable to borrow class", err))?
            .name().clone();
        Ok(name)
    }

    /// Returns the attribute with the given name or None
    pub fn attribute(&self, name: &str) -> Option<&AttributeFeature> {
        self.attributes.iter().find(|attr| attr.name == name)
    }

    /// Returns the super classes of the class
    pub fn super_classes(&self) -> &Vec<EClassRef> {
        &self.super_classes
    }

    /// Returns true if the class is abstract
    pub fn is_abstract(&self) -> bool {
        self.is_abstract
    }

    /// Returns the name of the class
    pub fn name(&self) -> &String {
        &self.name
    }

    /// Extends the class with the given super class.
    pub fn extend_class(&mut self, super_class: EClassRef) -> Result<(), Error> {
        self.super_classes
            .push(super_class);
        Ok(())
    }
}

// EClass
impl Display for EClass {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{} {{", self.name)?;
        for attribute in self.attributes.iter() {
            writeln!(f, "    {};", attribute)?;
        }
        write!(f, "}}")
    }
}


#[cfg(test)]
pub(in crate::ecore) mod test {
    use super::*;
    use crate::ecore::{data_type::{Relation, RelationType, Type}, feature::Bound};
    use std::rc::Rc;

    #[test]
    fn generate_user_and_car_classes() {

        //Openings
        let mut opening = EClass::new("Opening".to_string(), true);
        let opening_id = AttributeFeature::new_unique(
            "opening_ID".to_string(),
            false,
            Type::EString);
        opening.add_attribute(opening_id);
        let ref_opening = Rc::new(RefCell::new(opening));


        //Doors
        let mut door = EClass::new("Door".to_string(),false);
        let door_type = AttributeFeature::new_unique(
            "door_type".to_string(),
            false,
            Type::EString);        //side doors or car trunk
        door.extend_class(Rc::downgrade(&ref_opening)).unwrap();
        door.add_attribute(door_type);
        let ref_door =Rc::new(RefCell::new(door));


        //Windows
        let mut window = EClass::new("Window".to_string(), false);
        let window_type = AttributeFeature::new_unique(
            "window_type".to_string(),
            false,
            Type::EString);        //front, back or side windows
        let window_opacity = AttributeFeature::new_unique(
            "window_opacity".to_string(),
            false,
            Type::EString);    //opaque or transparent
        window.extend_class(Rc::downgrade(&ref_opening)).unwrap();
        window.add_attribute(window_type);
        window.add_attribute(window_opacity);
        let ref_window = Rc::new(RefCell::new(window));


        //Running
        let mut running = EClass::new("Running".to_string(), true);
        let running_id = AttributeFeature::new_unique(
            "running_ID".to_string(),
            false,
            Type::EString);
        running.add_attribute(running_id);
        let ref_running = Rc::new(RefCell::new(running));


        //Wheels
        let mut wheel = EClass::new("Wheel".to_string(), false);
        let wheel_size = AttributeFeature::new_unique(
            "wheel_size".to_string(),
            false,
            Type::EString);
        wheel.extend_class(Rc::downgrade(&ref_running)).unwrap();
        wheel.add_attribute(wheel_size);
        let ref_wheel = Rc::new(RefCell::new(wheel));


        //Color
        let mut color = EClass::new("Color".to_string(), false);
        let color_code = AttributeFeature::new_unique(
            "color_code".to_string(),
            false,
            Type::EString);
        color.add_attribute(color_code);
        let ref_color =Rc::new(RefCell::new(color));


        //Body
        let mut body = EClass::new("Body".to_string(), false);
        let body_color = AttributeFeature::new(
            "body_colors".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_color),
                RelationType::Reference {opposite: None }
        )));
        body.add_attribute(body_color);
        let ref_body = Rc::new(RefCell::new(body));


        //Experience
        let mut experience = EClass::new("Experience".to_string(), false);
        let exp_points = AttributeFeature::new_unique(
            "exp_points".to_string(),
            true,
            Type::EInt);
        experience.add_attribute(exp_points);
        let ref_experience = Rc::new(RefCell::new(experience));


        //Skills
        let mut skill = EClass::new("Skill".to_string(), false);
        let skill_exp = AttributeFeature::new(
            "skill_exp".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_experience),
                RelationType::Reference { opposite: None })));
        skill.add_attribute(skill_exp);
        let ref_skill = Rc::new(RefCell::new(skill));


        //Uniforms
        let mut uniform = EClass::new("Uniform".to_string(), false);
        let uniform_id= AttributeFeature::new_unique(
            "uniform_ID".to_string(),
            false,
            Type::EString);
        uniform.add_attribute(uniform_id);
        let ref_uniform = Rc::new(RefCell::new(uniform));


        //Drivers
        let mut driver = EClass::new("Driver".to_string(), false);
        let first_name = AttributeFeature::new_unique(
            "first_name".to_string(),
            false,
            Type::EString);
        let last_name = AttributeFeature::new_unique(
            "last_name".to_string(),
            false,
            Type::EString);
        let age = AttributeFeature::new_unique(
            "age".to_string(),
            true,
            Type::EInt);
        let driver_uniform = AttributeFeature::new_unique(
            "driver_uniform".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_uniform),
                RelationType::Reference { opposite: None })));
        let driver_skills = AttributeFeature::new_unbounded_list(
            "driver_skills".to_string(),
            true,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_skill),
                RelationType::Reference { opposite: None })));
        driver.add_attribute(first_name);
        driver.add_attribute(last_name);
        driver.add_attribute(age);
        driver.add_attribute(driver_uniform);
        driver.add_attribute(driver_skills);
        let ref_driver = Rc::new(RefCell::new(driver));


        //Sponsors
        let mut sponsor = EClass::new("Sponsor".to_string(), false);
        let company_name = AttributeFeature::new_unique(
            "company_name".to_string(),
            false,
            Type::EString);
        let sponsorship = AttributeFeature::new_unique(
            "sponsorship".to_string(),
            false,
            Type::EInt);
        sponsor.add_attribute(company_name);
        sponsor.add_attribute(sponsorship);
        let ref_sponsor = Rc::new(RefCell::new(sponsor));


        //Cars
        let mut car = EClass::new("Car".to_string(), false);
        let brand = AttributeFeature::new_unique(
            "brand".to_string(),
            false,
            Type::EString);
        let model = AttributeFeature::new_unique(
            "model".to_string(),
            false,
            Type::EString);
        let car_body = AttributeFeature::new_unique(
            "car_body".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_body),
                RelationType::Reference { opposite: None },)));
        let car_wheels = AttributeFeature::new(
            "car_wheels".to_string(),
            true,
            4,
            Bound::Finite(4),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_wheel),
                RelationType::Reference { opposite: None  },)));
        let car_door = AttributeFeature::new(
            "car_door".to_string(),
            true,
            5,
            Bound::Finite(5),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_door),
                RelationType::Reference { opposite: None  },)));
        let car_window = AttributeFeature::new(
            "car_window".to_string(),
            true,
            6,
            Bound::Finite(6),
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_window),
                RelationType::Reference { opposite: None  },)));
        let car_sponsor = AttributeFeature::new(
            "car_sponsor".to_string(),
            true,
            1,
            Bound::Unbounded,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_sponsor),
                RelationType::Reference { opposite: None },)));
        car.add_attribute(brand);
        car.add_attribute(model);
        car.add_attribute(car_body);
        car.add_attribute(car_wheels);
        car.add_attribute(car_door);
        car.add_attribute(car_window);
        car.add_attribute(car_sponsor);
        let ref_car = Rc::new(RefCell::new(car));
        //bidirectional
        let car_driver = AttributeFeature::new_unique(
            "car_driver".to_string(),
            true,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_driver),
                RelationType::Reference { opposite: Some("driver_car".to_string()) },)));
        let driver_car = AttributeFeature::new_unique(
            "driver_car".to_string(),
            false,
            Type::Relation(Relation::new(
                Rc::downgrade(&ref_car),
                RelationType::Reference { opposite: Some("car_driver".to_string()) })));
        ref_driver.borrow_mut().add_attribute(driver_car);
        ref_car.borrow_mut().add_attribute(car_driver);

    }

    pub struct PetriNet {
        pub orientation: Rc<RefCell<EClass>>,
        pub place: Rc<RefCell<EClass>>,
        pub edge: Rc<RefCell<EClass>>,
        pub transition: Rc<RefCell<EClass>>,
        pub petrinet: Rc<RefCell<EClass>>,
    }
    impl PetriNet {
        pub fn orientation_ref(&self) -> EClassRef {
            Rc::downgrade(&self.orientation)
        }
        pub fn place_ref(&self) -> EClassRef {
            Rc::downgrade(&self.place)
        }
        pub fn edge_ref(&self) -> EClassRef {
            Rc::downgrade(&self.edge)
        }
        pub fn transition_ref(&self) -> EClassRef {
            Rc::downgrade(&self.transition)
        }
        pub fn petrinet_ref(&self) -> EClassRef {
            Rc::downgrade(&self.petrinet)
        }
        pub fn new() -> Self {
            //Orientation
            let mut orientation = EClass::new("Orientation".to_string(), false);
            let transition_or_place = AttributeFeature::new_unique(
                "is_transition".to_string(),
                false,
                Type::EBoolean);
            orientation.add_attribute(transition_or_place);
            let ref_orientation= Rc::new(RefCell::new(orientation));


            //Place
            let mut place = EClass::new("Place".to_string(), false);
            let name = AttributeFeature::new_unique(
                "name".to_string(),
                false,
                Type::EString);
            let tokens = AttributeFeature::new_unique(
                "tokens".to_string(),
                true,
                Type::EInt);
            place.add_attribute(name);
            place.add_attribute(tokens);
            let ref_place= Rc::new(RefCell::new(place));


            //Edge
            let mut edge = EClass::new("Edge".to_string(), false);
            let weight = AttributeFeature::new_unique(
                "weight".to_string(),
                false,
                Type::EString);
            let orientation_type = AttributeFeature::new_unique(
                "orientation".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_orientation),
                    RelationType::Reference { opposite: None })));
            edge.add_attribute(weight);
            edge.add_attribute(orientation_type);
            let ref_edge= Rc::new(RefCell::new(edge));


            //Transition
            let mut transition = EClass::new("Transition".to_string(), false);
            let name = AttributeFeature::new_unique(
                "name".to_string(),
                false,
                Type::EString);
            transition.add_attribute(name);
            let ref_transition= Rc::new(RefCell::new(transition));



            //The relations between all classes
            //Place's relations
            let place_input_edges = AttributeFeature::new_unbounded_list(
                "input_edges".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_edge),
                    RelationType::Reference { opposite: None })));
            let place_output_edges = AttributeFeature::new_unbounded_list(
                "output_edges".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_edge),
                    RelationType::Reference { opposite: None })));
            ref_place.borrow_mut().add_attribute(place_input_edges);
            ref_place.borrow_mut().add_attribute(place_output_edges);


            //Edges' relations
            let edge_place = AttributeFeature::new_unique(
                "place".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_place),
                    RelationType::Reference { opposite: None })));
            let edge_transition =AttributeFeature::new_unique(
                "transition".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_transition),
                    RelationType::Reference { opposite: None })));
            ref_edge.borrow_mut().add_attribute(edge_place);
            ref_edge.borrow_mut().add_attribute(edge_transition);


            //Transition's relations
            let transition_input_edges = AttributeFeature::new_unbounded_list(
                "input_edges".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_edge),
                    RelationType::Reference { opposite: None })));
            let transition_output_edges = AttributeFeature::new_unbounded_list(
                "output_edges".to_string(),
                false,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_edge),
                    RelationType::Reference { opposite: None })));
            ref_transition.borrow_mut().add_attribute(transition_input_edges);
            ref_transition.borrow_mut().add_attribute(transition_output_edges);



            //Petrinet
            let mut petrinet = EClass::new("Petrinet".to_string(), false);
            let places = AttributeFeature::new_unbounded_list(
                "places".to_string(),
                true,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_place),
                    RelationType::Reference { opposite: None })));
            let edges = AttributeFeature::new_unbounded_list(
                "edges".to_string(),
                true,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_edge),
                    RelationType::Reference { opposite: None })));
            let transitions = AttributeFeature::new_unbounded_list(
                "transitions".to_string(),
                true,
                Type::Relation(Relation::new(
                    Rc::downgrade(&ref_transition),
                    RelationType::Reference { opposite: None })));
            petrinet.add_attribute(places);
            petrinet.add_attribute(edges);
            petrinet.add_attribute(transitions);

            let ref_petrinet = Rc::new(RefCell::new(petrinet));

            Self {
                orientation: ref_orientation,
                place: ref_place,
                edge: ref_edge,
                transition: ref_transition,
                petrinet: ref_petrinet,
            }
        }
    }

    #[test]
    fn generate_petri_net_class() {
        PetriNet::new();
    }

}
