use crate::ecore::data_value::Value;
use crate::ecore::debug::{BorrowedPlaceholder, RefMutPrinter, RefPrinter};
use std::cell::{BorrowError, Ref, RefCell};
use std::fmt::{Debug, Display, Formatter};
use std::ops::Deref;
use std::rc::{Rc, Weak};
use crate::ecore::data_type::RelationType;
use crate::ecore::eclass::EClassRef;

use crate::ecore::eobject::EObject;
use crate::ecore::error::Error;

#[derive(Debug)]
pub enum EObjectReference<'a> {
	Basic(&'a EObject),
	RefCell(Ref<'a, EObject>),
}

#[derive(Debug, Clone)]
pub enum RelationOwner {
	Exclusive(EObject),
	Const(Rc<EObject>),
	Mutable(Rc<RefCell<EObject>>),
}

#[derive(Debug, Clone)]
pub enum RelationOwnerRef<'a> {
	Exclusive(&'a EObject),
	Const(Rc<EObject>),
	Mutable(Rc<RefCell<EObject>>),
}

#[derive(Clone)]
pub enum RelationReference {
	Const(Weak<EObject>),
	Mutable(Weak<RefCell<EObject>>),
}

#[derive(Debug, Clone)]
pub enum RelationValue {
	Owner(RelationOwner),
	Reference(RelationReference),
}

impl EObjectReference<'_> {
	pub fn clone(this: &Self) -> Self {
		match this {
			EObjectReference::Basic(reference) => EObjectReference::Basic(reference),
			EObjectReference::RefCell(reference) => {
				EObjectReference::RefCell(Ref::clone(reference))
			}
		}
	}

	pub fn ptr_eq(first: &Self, second: &Self) -> bool {
		let reference_first = first.deref();
		let reference_second = second.deref();
		std::ptr::eq(reference_first, reference_second)
	}
}

impl Display for EObjectReference<'_> {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.deref())
	}
}

impl Deref for EObjectReference<'_> {
	type Target = EObject;

	fn deref(&self) -> &Self::Target {
		match self {
			EObjectReference::Basic(reference) => reference,
			EObjectReference::RefCell(reference) => reference.deref(),
		}
	}
}

// RelationOwner
impl RelationOwner {
    pub fn is_exclusive(&self) -> bool {
        matches!(self, RelationOwner::Exclusive(_))
    }
    pub fn is_const(&self) -> bool {
        matches!(self, RelationOwner::Const(_))
    }
    pub fn is_mutable(&self) -> bool {
        matches!(self, RelationOwner::Mutable(_))
    }
}
impl From<EObject> for RelationOwner {
    fn from(value: EObject) -> Self {
        RelationOwner::Exclusive(value)
    }
}
impl From<Rc<EObject>> for RelationOwner {
    fn from(value: Rc<EObject>) -> Self {
        RelationOwner::Const(value)
    }
}
impl From<Rc<RefCell<EObject>>> for RelationOwner {
    fn from(value: Rc<RefCell<EObject>>) -> Self {
        RelationOwner::Mutable(value)
    }
}

// RelationOwnerRef
impl RelationOwnerRef<'_> {
    pub fn is_exclusive(&self) -> bool {
        matches!(self, RelationOwnerRef::Exclusive(_))
    }
    pub fn is_const(&self) -> bool {
        matches!(self, RelationOwnerRef::Const(_))
    }
    pub fn is_mutable(&self) -> bool {
        matches!(self, RelationOwnerRef::Mutable(_))
    }
}
impl<'a> RelationOwnerRef<'a> {
	pub fn borrow(&'a self) -> Result<EObjectReference<'a>, BorrowError> {
		match self {
			RelationOwnerRef::Exclusive(reference) => Ok(EObjectReference::Basic(reference)),
			RelationOwnerRef::Const(reference) => Ok(EObjectReference::Basic(reference)),
			RelationOwnerRef::Mutable(reference) => {
				let uncelled = reference.try_borrow()?;
				Ok(EObjectReference::RefCell(uncelled))
			}
		}
	}
}

impl<'a> TryFrom<&'a Value> for RelationOwnerRef<'a> {
	type Error = Error;

	fn try_from(value: &'a Value) -> Result<Self, Self::Error> {
		if let Value::Relation(relation_value) = value {
			relation_value.upgrade()
		} else {
			Err(Error::new("Expected type Relation"))
		}
	}
}

impl<'a> From<&'a RelationOwner> for RelationOwnerRef<'a> {
	fn from(value: &'a RelationOwner) -> Self {
		match value {
			RelationOwner::Exclusive(data) => RelationOwnerRef::Exclusive(data),
			RelationOwner::Const(data) => RelationOwnerRef::Const(data.clone()),
			RelationOwner::Mutable(data) => RelationOwnerRef::Mutable(data.clone()),
		}
	}
}

impl Display for RelationOwnerRef<'_> {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			RelationOwnerRef::Exclusive(data) => {
				write!(f, "{}", data)
			}
			RelationOwnerRef::Const(data) => {
				write!(f, "{}", data)
			}
			RelationOwnerRef::Mutable(data_cell) => {
				if let Ok(data) = data_cell.try_borrow() {
					write!(f, "{}", data)
				} else {
					write!(f, "{}", BorrowedPlaceholder)
				}
			}
		}
	}
}

// RelationReference
impl RelationReference {
    pub fn new_invalid() -> Self {
        RelationReference::Const(Weak::new())
    }
    pub fn is_const(&self) -> bool {
        matches!(self, RelationReference::Const(_))
    }
    pub fn is_mutable(&self) -> bool {
        matches!(self, RelationReference::Mutable(_))
    }
	pub fn upgrade(&self) -> Result<RelationOwnerRef, Error> {
		match &self {
			RelationReference::Const(reference) => {
				let data = reference
					.upgrade()
					.ok_or_else(|| Error::new("Broken reference"))?;
				Ok(RelationOwnerRef::Const(data))
			}
			RelationReference::Mutable(reference) => {
				let data = reference
					.upgrade()
					.ok_or_else(|| Error::new("Broken reference"))?;
				Ok(RelationOwnerRef::Mutable(data))
			}
		}
	}
}

impl From<Weak<EObject>> for RelationReference {
    fn from(reference: Weak<EObject>) -> Self {
        RelationReference::Const(reference)
    }
}
impl From<Weak<RefCell<EObject>>> for RelationReference {
    fn from(reference: Weak<RefCell<EObject>>) -> Self {
        RelationReference::Mutable(reference)
    }
}

impl Debug for RelationReference {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match &self {
			RelationReference::Const(reference) => {
				let printer = RefPrinter::new(reference, |object, f| {
					let class = object.class();
					let printer = RefMutPrinter::new(&class, |class, f| {
						f.debug_struct("EClass")
							.field("name", &class.name())
							.finish_non_exhaustive()
					});
					f.debug_struct("EObject")
						.field("class", &printer)
						.finish_non_exhaustive()
				});
				f.debug_tuple("Const").field(&printer).finish()
			}
			RelationReference::Mutable(reference) => {
				let printer = RefMutPrinter::new(reference, |object, f| {
					let class = object.class();
					let printer = RefMutPrinter::new(&class, |class, f| {
						f.debug_struct("EClass")
							.field("name", &class.name())
							.finish_non_exhaustive()
					});
					f.debug_struct("EObject")
						.field("class", &printer)
						.finish_non_exhaustive()
				});
				f.debug_tuple("Mutable").field(&printer).finish()
			}
		}
	}
}

impl Display for RelationReference {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match &self {
			RelationReference::Const(reference) => {
				let printer = RefPrinter::new(reference, |obj, f| {
					let printer =
						RefMutPrinter::new(&obj.class(), |class, f| write!(f, "{}", class.name()));
					write!(f, "{} {{ .. }}", printer)
				});
				write!(f, "{}", printer)
			}
			RelationReference::Mutable(reference) => {
				let printer = RefMutPrinter::new(reference, |obj, f| {
					let printer =
						RefMutPrinter::new(&obj.class(), |class, f| write!(f, "{}", class.name()));
					write!(f, "{} {{ .. }}", printer)
				});
				write!(f, "{}", printer)
			}
		}
	}
}

// RelationValue
impl RelationValue {
	pub fn upgrade(&self) -> Result<RelationOwnerRef, Error> {
		match &self {
			RelationValue::Owner(value) => Ok(RelationOwnerRef::from(value)),
			RelationValue::Reference(reference) => reference.upgrade(),
		}
	}

	pub fn owned(&self) -> Result<RelationOwnerRef, RelationReference> {
		match &self {
			RelationValue::Owner(value) => Ok(RelationOwnerRef::from(value)),
			RelationValue::Reference(reference) => Err(reference.clone()),
		}
	}

	pub fn referenced(&self) -> Result<RelationReference, RelationOwnerRef> {
		match &self {
			RelationValue::Owner(value) => Err(RelationOwnerRef::from(value)),
			RelationValue::Reference(reference) => Ok(reference.clone()),
		}
	}

	pub fn is_owner(&self) -> bool {
		matches!(self, RelationValue::Owner(_))
	}

	pub fn is_reference(&self) -> bool {
		matches!(self, RelationValue::Reference(_))
	}

    // Note: this function doesn't return bidirectional information
    pub fn kind(&self) -> RelationType {
        match self {
            RelationValue::Owner(owned) => {
                RelationType::Owner { is_exclusive: owned.is_exclusive() }
            }
            RelationValue::Reference(_) => {
                // dummy opposite: cannot check
                RelationType::Reference { opposite: None }
            }
        }
    }
    pub fn class(&self) -> Result<EClassRef, Error> {
        let class = self.upgrade()?.borrow().map_err(|err| {
            Error::with_source("Unable to borrow class", err)
        })?.class();
        Ok(class)
    }
}

impl Display for RelationValue {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self.owned() {
			Ok(data) => {
				write!(f, "{}", data)
			}
			Err(reference) => {
				write!(f, "{}", reference)
			}
		}
	}
}

/*
#[cfg(test)]
mod test {
	use super::*;
	use crate::ecore::data_value::{ValueBounded, ConcreteValue};
	use crate::ecore::eclass;
	use crate::ecore::eclass::EClass;

	fn generate_user_eobject(
		class: &Rc<RefCell<EClass>>,
		first_name: &str,
		last_name: &str,
		age: u16,
	) -> EObject {
		EObject::new(
			Rc::downgrade(class),
			RelationReference::Const(Weak::new()),
			{
				let first_name = String::from(first_name);
				let first_name = Value::EString(first_name);
				let first_name = ValueBounded::Unique(first_name);
				let last_name = String::from(last_name);
				let last_name = Value::EString(last_name);
				let last_name = ValueBounded::Unique(last_name);
				let age = Value::EInt(age.into());
				let age = ValueBounded::Unique(age);

				let mut attributes = vec![];
				attributes.push(ConcreteValue::new(String::from("firstName"), first_name));
				attributes.push(ConcreteValue::new(String::from("lastName"), last_name));
				attributes.push(ConcreteValue::new(String::from("age"), age));
				attributes
			},
		)
	}

	#[test]
	fn test_eobject_reference() {
		let user_class = eclass::test::generate_user_eclass();
		let my_user1 = generate_user_eobject(&user_class, "John", "Smith", 42);
		let my_user2 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user3 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user4 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user4 = RefCell::new(my_user4);
		let my_user4_ref = my_user4.borrow();

		let my_user1_ref1 = EObjectReference::Basic(&my_user1);
		let my_user1_ref2 = EObjectReference::Basic(&my_user1);
		let my_user2_ref1 = EObjectReference::Basic(&my_user2);
		let my_user2_ref2 = EObjectReference::clone(&my_user2_ref1);
		let my_user3_ref = EObjectReference::Basic(&my_user3);
		let my_user4_ref1 = EObjectReference::RefCell(my_user4.borrow());
		let my_user4_ref2 = EObjectReference::RefCell(my_user4.borrow());
		let my_user4_ref3 = EObjectReference::Basic(my_user4_ref.deref());

		println!("my_user1_ref1: {}", my_user1_ref1);
		println!("my_user1_ref1: {:?}", my_user1_ref1);
		println!("my_user4_ref1: {}", my_user4_ref1);
		println!("my_user4_ref1: {:?}", my_user4_ref1);

		assert!(EObjectReference::ptr_eq(&my_user1_ref1, &my_user1_ref2));
		assert!(EObjectReference::ptr_eq(&my_user2_ref1, &my_user2_ref2));
		assert!(!EObjectReference::ptr_eq(&my_user1_ref1, &my_user2_ref1));
		assert!(!EObjectReference::ptr_eq(&my_user1_ref1, &my_user3_ref));
		assert!(EObjectReference::ptr_eq(&my_user4_ref1, &my_user4_ref2));
		assert!(EObjectReference::ptr_eq(&my_user4_ref1, &my_user4_ref3));
		assert!(EObjectReference::ptr_eq(&my_user4_ref2, &my_user4_ref3));
	}

	#[test]
	fn test_relation_owner_ref() {
		let user_class = eclass::test::generate_user_eclass();
		let my_user1 = generate_user_eobject(&user_class, "John", "Smith", 42);
		let my_user2 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user2 = Rc::new(my_user2);
		let my_user3 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user3 = Rc::new(RefCell::new(my_user3));
		let my_user4 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user4 = Rc::new(RefCell::new(my_user4));

		let my_user1_ref = RelationOwnerRef::Exclusive(&my_user1);
		let my_user2_ref = RelationOwnerRef::Const(my_user2.clone());
		let my_user3_ref = RelationOwnerRef::Mutable(my_user3.clone());
		let my_user4_ref = RelationOwnerRef::Mutable(my_user4.clone());

		let _ref = my_user3.borrow(); // user3 is readonly
		let _ref_mut = my_user4.borrow_mut(); // user4 is unusable

		println!("my_user1_ref: {}", my_user1_ref);
		println!("my_user1_ref: {:?}", my_user1_ref);
		println!("my_user2_ref: {}", my_user2_ref);
		println!("my_user2_ref: {:?}", my_user2_ref);
		println!("my_user3_ref: {}", my_user3_ref);
		println!("my_user3_ref: {:?}", my_user3_ref);
		println!("my_user4_ref: {}", my_user4_ref);
		println!("my_user4_ref: {:?}", my_user4_ref);

		assert!(my_user1_ref.borrow().is_ok());
		assert!(my_user2_ref.borrow().is_ok());
		assert!(my_user3_ref.borrow().is_ok());
		assert!(my_user4_ref.borrow().is_err());
	}

	#[test]
	fn test_relation_reference() {
		let user_class = eclass::test::generate_user_eclass();
		let my_user1 = generate_user_eobject(&user_class, "John", "Smith", 42);
		let my_user1 = Rc::new(my_user1);
		let my_user2 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user2 = Rc::new(my_user2);
		let my_user3 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user3 = Rc::new(RefCell::new(my_user3));
		// value is locked but accessible (upgrade is ok)
		let _ref_mut = my_user3.borrow_mut();

		let my_user1_ref = RelationReference::Const(Rc::downgrade(&my_user1));
		let my_user2_ref = RelationReference::Const(Rc::downgrade(&my_user2));
		let my_user3_ref = RelationReference::Mutable(Rc::downgrade(&my_user3));

		drop(my_user2); // oops user2 are removed

		println!("my_user1_ref: {}", my_user1_ref);
		println!("my_user1_ref: {:?}", my_user1_ref);
		println!("my_user2_ref: {}", my_user2_ref);
		println!("my_user2_ref: {:?}", my_user2_ref);
		println!("my_user3_ref: {}", my_user3_ref);
		println!("my_user3_ref: {:?}", my_user3_ref);

		assert!(my_user1_ref.upgrade().is_ok());
		assert!(my_user2_ref.upgrade().is_err());
		assert!(my_user3_ref.upgrade().is_ok());
	}

	#[test]
	fn test_relation_value() {
		let user_class = eclass::test::generate_user_eclass();
		let my_user1 = generate_user_eobject(&user_class, "John", "Smith", 42);
		let my_user2 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user2 = Rc::new(my_user2);
		let my_user3 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user3 = Rc::new(RefCell::new(my_user3));
		let my_user4 = generate_user_eobject(&user_class, "Foo", "Bar", 22);
		let my_user4 = Rc::new(RefCell::new(my_user4));

		let user1 = RelationValue::Owner(RelationOwner::Exclusive(my_user1));
		let user2 = RelationValue::Owner(RelationOwner::Const(my_user2.clone()));
		let user3 = RelationValue::Owner(RelationOwner::Mutable(my_user3.clone()));
		let user4 = RelationValue::Owner(RelationOwner::Mutable(my_user4.clone()));

		let user2_ref =
			RelationValue::Reference(RelationReference::Const(Rc::downgrade(&my_user2)));
		let user3_ref =
			RelationValue::Reference(RelationReference::Mutable(Rc::downgrade(&my_user3)));
		let user4_ref =
			RelationValue::Reference(RelationReference::Mutable(Rc::downgrade(&my_user4)));
		let user5_ref = RelationValue::Reference(RelationReference::Const(Weak::new())); // broken

		// owned data upgrade cannot fail
		assert!(user1.upgrade().is_ok());
		assert!(user2.upgrade().is_ok());
		assert!(user3.upgrade().is_ok());
		assert!(user4.upgrade().is_ok());
		// referenced data upgrade fail if data is unreachable
		assert!(user2_ref.upgrade().is_ok());
		assert!(user3_ref.upgrade().is_ok());
		assert!(user4_ref.upgrade().is_ok());
		assert!(user5_ref.upgrade().is_err());

		assert!(user1.owned().is_ok());
		assert!(user2.owned().is_ok());
		assert!(user3.owned().is_ok());
		assert!(user4.owned().is_ok());
		assert!(user2_ref.owned().is_err());
		assert!(user3_ref.owned().is_err());
		assert!(user4_ref.owned().is_err());
		assert!(user5_ref.owned().is_err());

		assert!(user1.referenced().is_err());
		assert!(user2.referenced().is_err());
		assert!(user3.referenced().is_err());
		assert!(user4.referenced().is_err());
		assert!(user2_ref.referenced().is_ok());
		assert!(user3_ref.referenced().is_ok());
		assert!(user4_ref.referenced().is_ok());
		assert!(user5_ref.referenced().is_ok());
	}
}
 */
