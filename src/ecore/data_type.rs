use crate::ecore::debug::RefMutPrinter;
use crate::ecore::eclass::EClassRef;
use std::fmt::{Debug, Display, Formatter};

/// Describe a relation between two EClasses.
/// An [`EClass`] either contains or knows another one.
/// In the first case, this is an `Owner` type relationship, in the second case, this is a `Reference` type relationship.
/// A relation where the [`EClass`] is the unique owner of the other one is called Exclusive.
/// A relation can be bidirectional, i.e. the other [`EClass`] also knows the first one. In this case, the `opposite` field is set.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum RelationType {
	Owner {
		is_exclusive: bool,
	},
	Reference {
        opposite: Option<String>,
	},
}

/// A relation between two [`EClass`]es.
/// The `target_type` field is the [`EClass`] that is either owned or referenced.
/// The `relation_type` field describes the type of the relation.
#[derive(Clone)]
pub struct Relation {
	target_type: EClassRef,
	relation_type: RelationType,
}

impl Relation {
	/// Create a new [`Relation`].
	/// # Arguments
	/// * `target_type` - The [`EClass`] that is either owned or referenced.
	/// * `relation_type` - The type of the relation.
	pub fn new(target_type: EClassRef, relation_type: RelationType) -> Self {
		Self {
			target_type,
			relation_type,
		}
	}

	/// Get the [`EClass`] that is either owned or referenced.
	pub fn target_type(&self) -> EClassRef {
		self.target_type.clone()
	}

	/// Get the type of the relation.
	pub fn relation_type(&self) -> &RelationType {
		&self.relation_type
	}
}

/// A feature of an [`EClass`]. This can be an primitive type or a reference.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Type {
	EBoolean,
	EByte,
	EChar,
	EDouble,
	EFloat,
	EInt,
	ELong,
	EShort,
	EString,
	Relation(Relation),
}

// RelationType
impl RelationType {
    pub fn is_compatible(first: &Self, second: &Self) -> bool {
        match (first, second) {
            (RelationType::Owner { is_exclusive: first_exclusive },
                RelationType::Owner { is_exclusive: second_exclusive }) => {
                first_exclusive == second_exclusive
            },
            (RelationType::Reference { .. },
                RelationType::Reference { .. }) => {
                true
            },
            _ => false
        }
    }
}
impl Display for RelationType {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			RelationType::Owner { is_exclusive } => {
				if *is_exclusive {
					write!(f, "Exclusive")
				} else {
					write!(f, "Owner")
				}
			}
			RelationType::Reference { opposite } => {
				if let Some(opposite_name) = opposite {
					write!(f, "Bidirectional Reference (with {opposite_name})")
				} else {
					write!(f, "Reference")
				}
			}
		}
	}
}

// Relation
impl Debug for Relation {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let ty_printer = RefMutPrinter::new(&self.target_type, |this, f| {
			f.debug_struct("EClass")
				.field("name", &this.name())
				.finish_non_exhaustive()
		});
		f.debug_struct("Relation")
			.field("target_type", &ty_printer)
			.field("relation_type", &self.relation_type)
			.finish()
	}
}

impl Display for Relation {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let ty_printer =
			RefMutPrinter::new(&self.target_type, |this, f| write!(f, "{}", this.name()));
		write!(f, "{}({})", ty_printer, self.relation_type)
	}
}

impl PartialEq for Relation {
	fn eq(&self, other: &Self) -> bool {
		self.target_type.ptr_eq(&other.target_type) && self.relation_type == other.relation_type
	}
}

impl Eq for Relation {}

// Type
impl Type {
	pub fn new_owner(class: EClassRef) -> Self {
		Type::Relation(Relation {
			target_type: class,
			relation_type: RelationType::Owner {
				is_exclusive: false,
			},
		})
	}

	pub fn new_exclusive(class: EClassRef) -> Self {
		Type::Relation(Relation {
			target_type: class,
			relation_type: RelationType::Owner { is_exclusive: true },
		})
	}

	pub fn new_ref(class: EClassRef) -> Self {
		Type::Relation(Relation {
			target_type: class,
			relation_type: RelationType::Reference {
				opposite: None,
			},
		})
	}

	pub fn new_biref(class: EClassRef, attr_name: String) -> Self {
		Type::Relation(Relation {
			target_type: class,
			relation_type: RelationType::Reference {
				opposite: Some(attr_name),
			},
		})
	}

    pub fn relation(self) -> Result<Relation, Self> {
        match self {
            Type::Relation(relation) => Ok(relation),
            _ => Err(self),
        }
    }
    pub fn relation_ref(&self) -> Result<&Relation, &Self> {
        match self {
            Type::Relation(relation) => Ok(relation),
            _ => Err(self),
        }
    }
}

impl Display for Type {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			Type::EBoolean => write!(f, "Boolean"),
			Type::EByte => write!(f, "Byte"),
			Type::EChar => write!(f, "Char"),
			Type::EDouble => write!(f, "Double"),
			Type::EFloat => write!(f, "Float"),
			Type::EInt => write!(f, "Int"),
			Type::ELong => write!(f, "Long"),
			Type::EShort => write!(f, "Short"),
			Type::EString => write!(f, "String"),
			Type::Relation(relation) => write!(f, "{}", relation),
		}
	}
}

/*
// Tests
#[cfg(test)]
mod test {
	use super::*;
	use crate::ecore::eclass::test::generate_empty_eclass;
	use crate::ecore::eclass::test::generate_recursive_eclass;
	use crate::ecore::eclass::test::generate_user_eclass;
	use std::rc::Rc;

	#[test]
	fn test_primitive_type() {
		let my_int = Type::EInt;
		let my_string = Type::EString;

		assert_eq!(format!("{}", my_int), "Int");
		assert_eq!(format!("{}", my_string), "String");
	}

	#[test]
	fn test_relation_type() {
		let empty_eclass_owner = generate_empty_eclass();
		let empty_eclass = Rc::downgrade(&empty_eclass_owner);

		let type_owner_new = Type::new_owner(empty_eclass.clone());
		let type_ref_new = Type::new_ref(empty_eclass.clone());
		let type_exclusive_new = Type::new_exclusive(empty_eclass.clone());
		let type_biref_new = Type::new_biref(empty_eclass.clone());

		let type_owner = Type::Relation(Relation {
			target_type: empty_eclass.clone(),
			relation_type: RelationType::Owner {
				is_exclusive: false,
			},
		});
		let type_ref = Type::Relation(Relation {
			target_type: empty_eclass.clone(),
			relation_type: RelationType::Reference {
				is_bidirectional: false,
			},
		});
		let type_exclusive = Type::Relation(Relation {
			target_type: empty_eclass.clone(),
			relation_type: RelationType::Owner { is_exclusive: true },
		});
		let type_biref = Type::Relation(Relation {
			target_type: empty_eclass.clone(),
			relation_type: RelationType::Reference {
				is_bidirectional: true,
			},
		});

		assert_eq!(type_owner_new, type_owner);
		assert_eq!(type_ref_new, type_ref);
		assert_eq!(type_exclusive_new, type_exclusive);
		assert_eq!(type_biref_new, type_biref);
	}

	#[test]
	fn test_relation_type_display() {
		let relation_exclusive = RelationType::Owner { is_exclusive: true };
		let relation_ref = RelationType::Reference {
			is_bidirectional: false,
		};

		assert_eq!(format!("{}", relation_exclusive), "Exclusive");
		assert_eq!(format!("{}", relation_ref), "Reference");

		let class_empty = generate_empty_eclass();
		let class_user = generate_user_eclass();

		let relation_user_exclusive = Type::Relation(Relation {
			target_type: Rc::downgrade(&class_user),
			relation_type: relation_exclusive,
		});
		let relation_empty_reference = Type::Relation(Relation {
			target_type: Rc::downgrade(&class_empty),
			relation_type: relation_ref,
		});
		let relation_user_bidirectional_reference = Type::Relation(Relation {
			target_type: Rc::downgrade(&class_user),
			relation_type: RelationType::Reference {
				is_bidirectional: true,
			},
		});

		assert_eq!(format!("{}", relation_user_exclusive), "User(Exclusive)");
		assert_eq!(format!("{}", relation_empty_reference), "Empty(Reference)");
		assert_eq!(
			format!("{}", relation_user_bidirectional_reference),
			"User(Bidirectional Reference)"
		);
	}

	#[test]
	fn test_type_display() {
		let user_self = generate_recursive_eclass();
		let user_self_ref = user_self.borrow();
		let friend_field = user_self_ref
			.attributes
			.iter()
			.find(|f| f.name == "friends")
			.expect("Field `friends` not found in class")
			.attr_type.clone();

		drop(user_self_ref);

		assert_eq!(format!("{}", friend_field), "User(Reference)");
		assert_eq!(
			format!("{:?}", friend_field),
			concat!(
				"Relation(Relation { target_type: Weak { ",
				"value: RefCell { value: EClass { name: \"User\", .. } } },",
				" relation_type: Reference { is_bidirectional: false } })"
			)
		);

		let _user_self_ref_mut = user_self.borrow_mut();
		assert_eq!(format!("{}", friend_field), "<borrowed>(Reference)");
		assert_eq!(
			format!("{:?}", friend_field),
			concat!(
				"Relation(Relation { target_type:",
				" Weak { value: RefCell { value: <borrowed> } },",
				" relation_type: Reference { is_bidirectional: false } })"
			)
		);
	}

	#[test]
	fn test_type_eq_basic() {
		let user_eclass = generate_user_eclass();
		let user_eclass_ref = user_eclass.borrow();

		let first_name = user_eclass_ref
			.attributes
			.iter()
			.find(|f| f.name == "firstName")
			.expect("Field `firstName` not found in class")
			.attr_type.clone();

		let last_name = user_eclass_ref
			.attributes
			.iter()
			.find(|f| f.name == "lastName")
			.expect("Field `lastName` not found in class")
			.attr_type.clone();

		let age = user_eclass_ref
			.attributes
			.iter()
			.find(|f| f.name == "age")
			.expect("Field `age` not found in class")
			.attr_type.clone();

		assert_eq!(first_name, first_name); // self
		assert_eq!(first_name, last_name); // same type
		assert_ne!(first_name, age); // different type
		assert_ne!(last_name, age); // different type
	}

	#[test]
	fn test_type_eq_ref() {
		let user_first = generate_recursive_eclass();
		let user_second = generate_recursive_eclass();
		let user_first_ref = user_first.borrow();
		let user_second_ref = user_second.borrow();

		let friend_first = user_first_ref
			.attributes
			.iter()
			.find(|f| f.name == "friends")
			.expect("Field `friends` not found in class")
			.clone();

		let friend_second = user_second_ref
			.attributes
			.iter()
			.find(|f| f.name == "friends")
			.expect("Field `friends` not found in class")
			.clone();

		let friend_first_clone = friend_first.clone();

		// self
		assert_eq!(friend_first, friend_first);
		// reference to identical class but not the same
		assert_ne!(friend_first, friend_second);
		// reference to the same class
		assert_eq!(friend_first, friend_first_clone);
		// reference to identical class but not the same
		assert_ne!(friend_second, friend_first_clone);
	}
}
 */
