use std::error::Error as ErrorTrait;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub struct Error {
	message: String,
	source: Option<Box<dyn ErrorTrait + 'static>>,
}

impl Error {
	pub fn new<T: Display>(message: T) -> Self {
		Self {
			message: message.to_string(),
			source: None,
		}
	}
	pub fn with_source<T, E>(message: T, source: E) -> Self
	where
		T: Display,
		E: ErrorTrait + 'static,
	{
		Self {
			message: message.to_string(),
			source: Some(Box::new(source)),
		}
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		if let Some(source) = &self.source {
			write!(f, "{}: {}", self.message, source)
		} else {
			write!(f, "{}", self.message)
		}
	}
}

impl ErrorTrait for Error {
	fn source(&self) -> Option<&(dyn ErrorTrait + 'static)> {
		if let Some(source) = &self.source {
			Some(source.as_ref())
		} else {
			None
		}
	}
}
