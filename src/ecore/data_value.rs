use std::cell::RefCell;
use crate::ecore::data_relation::{RelationOwner, RelationReference, RelationValue};
use crate::ecore::error::Error;
use std::fmt::{Display, Formatter};
use std::rc::{Rc, Weak};
use crate::ecore::data_type::{Relation, Type};
use crate::ecore::eobject::EObject;

#[derive(Debug, Clone)]
pub enum Value {
    EBoolean(bool),
    EByte(i8),
    EChar(char),
    EDouble(f64),
    EFloat(f32),
    EInt(i32),
    ELong(i64),
    EShort(i16),
    EString(String),
    Relation(RelationValue),
}

#[derive(Debug, Clone)]
pub enum ValueBounded {
    Unique(Value),         // 1-1
    Option(Option<Value>), // 0-1
    List(Vec<Value>),      // m-n
}

// Value
impl Value {
    pub fn type_of(&self) -> Result<Type, Error> {
        let ty = match self {
            Value::EBoolean(_) => Type::EBoolean,
            Value::EByte(_) => Type::EByte,
            Value::EChar(_) => Type::EChar,
            Value::EDouble(_) => Type::EDouble,
            Value::EFloat(_) => Type::EFloat,
            Value::EInt(_) => Type::EInt,
            Value::ELong(_) => Type::ELong,
            Value::EShort(_) => Type::EShort,
            Value::EString(_) => Type::EString,
            Value::Relation(relation) => {
                Type::Relation(Relation::new(relation.class()?, relation.kind()))
            }
        };
        Ok(ty)
    }
}

macro_rules! impl_try_from_value_for {
    ($field:ident, $ty:ty) => {
        impl TryFrom<&Value> for $ty {
            type Error = Error;

            fn try_from(value: &Value) -> Result<Self, Self::Error> {
                if let &Value::$field(typed_value) = value {
                    Ok(typed_value)
                } else {
                    Err(Error::new(concat!("Expected type ", stringify!($ty))))
                }
            }
        }
    };
}
macro_rules! impl_from_for_value {
    ($field:ident, $ty:ty) => {
        impl From<$ty> for Value {
            fn from(value: $ty) -> Self {
                Value::$field(value)
            }
        }
    };
}

impl_try_from_value_for!(EBoolean, bool);
impl_try_from_value_for!(EInt, i32);
impl_try_from_value_for!(ELong, i64);
impl_try_from_value_for!(EShort, i16);
impl_try_from_value_for!(EByte, i8);
impl_try_from_value_for!(EFloat, f32);
impl_try_from_value_for!(EDouble, f64);
impl_try_from_value_for!(EChar, char);

impl TryFrom<&Value> for String {
    type Error = Error;

    fn try_from(value: &Value) -> Result<Self, Self::Error> {
        if let Value::EString(typed_value) = value {
            Ok(typed_value.clone())
        } else {
            Err(Error::new("Expected type String"))
        }
    }
}

impl<'a> TryFrom<&'a Value> for &'a String {
    type Error = Error;

    fn try_from(value: &'a Value) -> Result<Self, Self::Error> {
        if let Value::EString(typed_value) = value {
            Ok(typed_value)
        } else {
            Err(Error::new("Expected type String"))
        }
    }
}

impl_from_for_value!(EBoolean, bool);
impl_from_for_value!(EInt, i32);
impl_from_for_value!(ELong, i64);
impl_from_for_value!(EShort, i16);
impl_from_for_value!(EByte, i8);
impl_from_for_value!(EFloat, f32);
impl_from_for_value!(EDouble, f64);
impl_from_for_value!(EChar, char);

impl From<&String> for Value {
    fn from(value: &String) -> Self {
        Value::EString(value.clone())
    }
}
impl From<&str> for Value {
    fn from(value: &str) -> Self {
        Value::EString(String::from(value))
    }
}
impl From<String> for Value {
    fn from(value: String) -> Self {
        Value::EString(value)
    }
}
impl From<EObject> for Value {
    fn from(value: EObject) -> Self {
        Value::Relation(RelationValue::Owner(RelationOwner::from(value)))
    }
}
impl From<Rc<EObject>> for Value {
    fn from(value: Rc<EObject>) -> Self {
        Value::Relation(RelationValue::Owner(RelationOwner::from(value)))
    }
}
impl From<Rc<RefCell<EObject>>> for Value {
    fn from(value: Rc<RefCell<EObject>>) -> Self {
        Value::Relation(RelationValue::Owner(RelationOwner::from(value)))
    }
}
impl From<Weak<EObject>> for Value {
    fn from(value: Weak<EObject>) -> Self {
        Value::Relation(RelationValue::Reference(RelationReference::from(value)))
    }
}
impl From<Weak<RefCell<EObject>>> for Value {
    fn from(value: Weak<RefCell<EObject>>) -> Self {
        Value::Relation(RelationValue::Reference(RelationReference::from(value)))
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::EBoolean(value) => write!(f, "{}", value),
            Value::EInt(value) => write!(f, "{}", value),
            Value::ELong(value) => write!(f, "{}", value),
            Value::EShort(value) => write!(f, "{}", value),
            Value::EByte(value) => write!(f, "{}", value),
            Value::EFloat(value) => write!(f, "{}", value),
            Value::EDouble(value) => write!(f, "{}", value),
            Value::EChar(value) => write!(f, "{}", value),
            Value::EString(value) => write!(f, "{}", value),
            Value::Relation(relation) => write!(f, "{}", relation),
        }
    }
}

// ValueBounded
impl Display for ValueBounded {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ValueBounded::Unique(value) => write!(f, "{}", value),
            ValueBounded::Option(Some(value)) => write!(f, "Some({})", value),
            ValueBounded::Option(None) => write!(f, "None"),
            ValueBounded::List(values) => {
                write!(f, "[")?;
                for value in values {
                    write!(f, "{}, ", value)?;
                }
                write!(f, "]")
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_convert_primitive() {
        let my_int32 = Value::EInt(-42);
        let my_uint64 = Value::ELong(25);
        let my_str1 = Value::EString(String::from("test"));
        let my_str2 = Value::EString(String::from("FOO"));

        let int32 = i32::try_from(&my_int32);
        let int64: Result<i64, _> = (&my_uint64).try_into();
        let str1 = String::try_from(&my_str1);
        let str2: Result<&String, _> = (&my_str2).try_into();

        assert!(int32.is_ok());
        assert!(int64.is_ok());
        assert!(str1.is_ok());
        assert!(str2.is_ok());

        let failed_int32 = i32::try_from(&my_uint64);
        let failed_string = String::try_from(&my_int32);
        let failed_string_ref = <&String>::try_from(&my_int32);

        assert!(failed_int32.is_err());
        assert!(failed_string.is_err());
        assert!(failed_string_ref.is_err());

        let str: &str = "test";
        let string: String = String::from("foo");
        let int: i32 = 42;
        let boolean: bool = true;

        assert!(matches!(Value::from(str), Value::EString(val) if val == *"test"));
        assert!(matches!(Value::from(&string), Value::EString(val) if val == *"foo"));
        assert!(matches!(Value::from(string), Value::EString(val) if val == *"foo"));
        assert!(matches!(Value::from(int), Value::EInt(val) if val == 42));
        assert!(matches!(Value::from(boolean), Value::EBoolean(val) if val));
    }
    #[test]
    fn test_display_primitive() {
        let boolean = false;
        let int32 = -42;
        let uint64 = 25;
        let str1 = String::from("test");

        let my_boolean = Value::EBoolean(boolean);
        let my_int32 = Value::EInt(int32);
        let my_uint64 = Value::ELong(uint64);
        let my_str1 = Value::EString(str1.clone());

        assert_eq!(format!("{}", my_boolean), format!("{}", boolean));
        assert_eq!(format!("{}", my_int32), format!("{}", int32));
        assert_eq!(format!("{}", my_uint64), format!("{}", uint64));
        assert_eq!(format!("{}", my_str1), format!("{}", str1));
    }
}
