use proc_macro2::Span;
use quote::ToTokens;
use syn::punctuated::Punctuated;
use syn::spanned::Spanned;
use syn::token::Comma;
use syn::{Lit, Member, Meta, NestedMeta, Path};

type AttrArgs = Punctuated<NestedMeta, Comma>;

pub(crate) struct Property {
    pub(crate) inner_crate: bool,
    pub(crate) notifier: Member,
}

impl Property {
    // extern attributes
    fn parse_inner_crate(&mut self, _span: Span, value: Option<Lit>) -> syn::Result<()> {
        self.inner_crate = value.map_or(Ok(true), |lit| {
            if let Lit::Bool(lit_bool) = lit {
                Ok(lit_bool.value())
            } else {
                let msg = format!("Expected boolean found `{}`", lit.to_token_stream());
                Err(syn::Error::new(lit.span(), msg))
            }
        })?;
        Ok(())
    }

    // intern attributes
    fn parse_notifier(
        &mut self,
        _span: Span,
        value: Option<Lit>,
        member: &Member,
    ) -> syn::Result<()> {
        self.notifier = value.map_or_else(
            || Ok(member.clone()),
            |lit| {
                let message = format!("Unexpected `{}`", lit.to_token_stream());
                Err(syn::Error::new(lit.span(), message))
            },
        )?;
        Ok(())
    }

    // parse one extern attribute
    fn parse_attr_extern(&mut self, name: Path, value: Option<Lit>) -> syn::Result<()> {
        if name.is_ident("inner_crate") {
            self.parse_inner_crate(name.span(), value)
        } else {
            let msg = format!("unknown option {}", name.to_token_stream());
            Err(syn::Error::new(name.span(), &msg))
        }
    }

    // parse one intern attribute
    fn parse_attr_intern(
        &mut self,
        name: Path,
        value: Option<Lit>,
        member: &Member,
    ) -> syn::Result<()> {
        if name.is_ident("notifier") {
            self.parse_notifier(name.span(), value, member)
        } else {
            let msg = format!("unknown option {}", name.to_token_stream());
            Err(syn::Error::new(name.span(), &msg))
        }
    }

    // parse extern attribute list
    pub(crate) fn parse_attr_list_extern(&mut self, args: AttrArgs) -> syn::Result<()> {
        args.into_iter().try_for_each(|attr| match attr {
            NestedMeta::Meta(meta) => {
                match meta {
                    Meta::Path(path) => self.parse_attr_extern(path, None),
                    Meta::List(list) => Err(syn::Error::new(list.span(), "Invalid argument")),
                    Meta::NameValue(name_value) => {
                        self.parse_attr_extern(
                            name_value.path, // name
                            Some(name_value.lit),
                        ) // value
                    }
                }
            }
            NestedMeta::Lit(lit) => Err(syn::Error::new(lit.span(), "Invalid argument")),
        })
    }

    // parse intern attribute list
    pub(crate) fn parse_attr_list_intern(
        &mut self,
        args: AttrArgs,
        member: &Member,
    ) -> syn::Result<()> {
        args.into_iter().try_for_each(|attr| match attr {
            NestedMeta::Meta(meta) => {
                match meta {
                    Meta::Path(path) => self.parse_attr_intern(path, None, member),
                    Meta::List(list) => Err(syn::Error::new(list.span(), "Invalid argument")),
                    Meta::NameValue(name_value) => {
                        self.parse_attr_intern(
                            name_value.path,      // name
                            Some(name_value.lit), // value
                            member,
                        ) // associated member
                    }
                }
            }
            NestedMeta::Lit(lit) => Err(syn::Error::new(lit.span(), "Invalid argument")),
        })
    }
}
