mod observable_derive;
mod observable_derive_property;

use proc_macro::TokenStream;

use crate::observable_derive::{generate, parse};
use syn::{parse_macro_input, DeriveInput};

/// Implement `ObservableDyn`/`Observable` for type.
///
/// # Attributes
/// ## `observable(inner_crate)`
/// Reserved for intern usage: you must set `true` value when
/// type is inside capstone crate, and `false` otherwise.
///
/// This is an external parameter, it should be on the type.
///
/// If this attribute is missing, its value is `false` and if this attribute is
/// present, its value is `true` (`#[observable(inner_crate)]`).
/// You can also explicitly set the value
/// (`#[observable(inner_crate = false)]`)
///
/// ## `observable(notifier)`
/// Indicates to `Observer` derive macro which variable is the `Notifier`.
///
/// The indicated variable has not necessarily `Notifier` type
/// but must implement `ObservableDyn`
///
/// This is an internal parameter, it should be on the field.
/// You can define lifetime attribute like this:
/// ```ignore
/// #[derive(Observable)]
/// struct Foo {
///     #[observable(notifier)] notifier: Notifier, // <- marked as notifier
/// }
/// ```
#[proc_macro_derive(Observable, attributes(observable))]
pub fn derive_observable(item: TokenStream) -> TokenStream {
    // syn parse
    let input_derive = parse_macro_input!(item as DeriveInput);

    // parse
    let input = match parse(input_derive) {
        Ok(input_struct) => input_struct,
        Err(error) => return error.to_compile_error().into(),
    };

    // generation
    generate(input).into()
}
