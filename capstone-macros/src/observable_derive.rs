use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;

use syn::spanned::Spanned;
use syn::{
    AttrStyle, Attribute, Data, DeriveInput, Field, Generics, Index, Member, Meta,
};

use crate::observable_derive_property::Property;

pub(crate) struct Input {
    property: Property,
    name: Ident,
    generics: Generics,
}

fn parse_attr_extern(
    property: &mut Property,
    attributes: impl Iterator<Item = Attribute>,
) -> syn::Result<()> {
    attributes
        .filter(|attr| matches!(attr.style, AttrStyle::Outer) && attr.path.is_ident("observable"))
        .map(|attr| attr.parse_meta())
        .try_for_each(|attr_result| match attr_result {
            Ok(Meta::List(list)) => property.parse_attr_list_extern(list.nested),
            Ok(Meta::NameValue(name_value)) => {
                let msg = concat!(
                    "expected attribute format `observable(...)`, ",
                    "found format `observable = ...`"
                );
                Err(syn::Error::new(name_value.span(), msg))
            }
            Ok(Meta::Path(path)) => {
                let msg = concat!(
                    "expected attribute format `observable(...)`, ",
                    "found format `observable`"
                );
                Err(syn::Error::new(path.span(), msg))
            }
            Err(error) => {
                println!("Meta error: {}", error);
                Err(error)
            }
        })
}

fn parse_attr_intern<'a, T>(property: &mut Property, attributes: T) -> syn::Result<()>
where
    T: Iterator<Item = (Member, &'a Attribute)>,
{
    attributes
        .filter(|(_, attr)| attr.path.is_ident("observable"))
        .map(|(member, attr)| (member, attr.style, attr.parse_meta()))
        .try_for_each(|(member, style, attr_result)| match attr_result {
            Ok(Meta::List(list)) => match style {
                AttrStyle::Outer => property.parse_attr_list_intern(list.nested, &member),
                AttrStyle::Inner(_) => property.parse_attr_list_extern(list.nested),
            },
            Ok(Meta::NameValue(name_value)) => {
                let msg = concat!(
                    "expected attribute format `observable(...)`, ",
                    "found format `observable = ...`"
                );
                Err(syn::Error::new(name_value.span(), msg))
            }
            Ok(Meta::Path(path)) => {
                let msg = concat!(
                    "expected attribute format `observable(...)`, ",
                    "found format `observable`"
                );
                Err(syn::Error::new(path.span(), msg))
            }
            Err(error) => Err(error),
        })
}

pub(crate) fn parse(input: DeriveInput) -> syn::Result<Input> {
    let mut property = Property {
        inner_crate: false,
        notifier: Member::Named(Ident::new("notifier", Span::call_site())),
    };

    // extern attributes
    parse_attr_extern(&mut property, input.attrs.into_iter())?;

    // intern attributes
    match input.data {
        Data::Struct(struct_ty) => {
            let attributes = struct_ty
                .fields
                .iter()
                .enumerate()
                .flat_map(|(index, field)| {
                    let Field { attrs, ident, .. } = field;
                    let member = match ident {
                        None => Member::Unnamed(Index {
                            index: u32::try_from(index).expect("Field index does not fit in u32"),
                            span: field.span(),
                        }),
                        Some(ident) => Member::Named(ident.clone()),
                    };
                    attrs.into_iter().map(move |attr| (member.clone(), attr))
                });
            parse_attr_intern(&mut property, attributes)
        }
        Data::Enum(enum_ty) => {
            let msg = "enum type can not derive Observable automatically";
            Err(syn::Error::new(enum_ty.enum_token.span(), msg))
        }
        Data::Union(union_ty) => {
            let msg = "union type can not derive Observable automatically";
            Err(syn::Error::new(union_ty.union_token.span(), msg))
        }
    }?;

    Ok(Input {
        property,
        name: input.ident,
        generics: input.generics,
    })
}

pub(crate) fn generate(input: Input) -> TokenStream {
    let Input {
        property:
            Property {
                inner_crate,
                notifier,
            },
        name,
        generics,
    } = input;

    let (
        observable,
        adapter_id,
        adapter_fn,
        adapter_fn_mut,
        notification,
        feature,
    ) = if inner_crate {
        (
            quote!(crate::notify::notifier::ObservableDyn),
            quote!(crate::notify::notifier::AdapterId),
            quote!(crate::notify::notifier::AdapterFnDyn),
            quote!(crate::notify::notifier::AdapterFnMutDyn),
            quote!(crate::notify::notification::Notification),
            quote!(crate::ecore::feature::AttributeFeature),
        )
    } else {
        (
            quote!(::capstone::notify::notifier::ObservableDyn),
            quote!(::capstone::notify::notifier::AdapterId),
            quote!(::capstone::notify::notifier::AdapterFnDyn),
            quote!(::capstone::notify::notifier::AdapterFnMutDyn),
            quote!(::capstone::notify::notification::Notification),
            quote!(::capstone::ecore::feature::AttributeFeature),
        )
    };

    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    quote!(
        impl #impl_generics #observable
        for #name #ty_generics #where_clause
        {
            fn register_mut_dyn(&mut self, adapter: #adapter_fn_mut) -> #adapter_id
            {
                self.#notifier.register_mut_dyn(adapter)
            }

            fn unregister_mut(&mut self, id: #adapter_id)
            {
                self.#notifier.unregister_mut(id)
            }

            fn register_dyn(&mut self, adapter: #adapter_fn) -> #adapter_id
            {
                self.#notifier.register_dyn(adapter)
            }

            fn unregister(&mut self, id: #adapter_id)
            {
                self.#notifier.unregister(id)
            }

            fn deliver(&self) -> ::core::primitive::bool
            {
                self.#notifier.deliver()
            }

            fn set_deliver(&mut self, deliver: ::core::primitive::bool)
            {
                self.#notifier.set_deliver(deliver)
            }

            fn notify_dyn(
                &self,
                origin: &dyn #observable,
                feature: ::core::option::Option<&#feature>,
                notif: #notification
            )
            {
                self.#notifier.notify_dyn(origin, feature, notif)
            }
        }
    )
}
